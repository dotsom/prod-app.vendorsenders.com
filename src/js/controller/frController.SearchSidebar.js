(function($){
  $.fn.searchFilterIcons = function(handlers){

    // Create empty elements to house filters and results
    $(this).parent().append('<div id="filterWrap"></div><ul id="resultsWrap"></ul>');

    // Store all icons and wrappers for later use
    var $icons = $(this).children('li'),
      $iconSet = $(this),
      $filterWrap = $('#filterWrap'),
      $resultsWrap = $('#resultsWrap');

    // Bind icon switching
    $icons.click(function(e){
      e.preventDefault();
      $icons.removeClass('selected');
      $(this).addClass('selected');
      $iconSet.attr('current-filter', $(this).attr('filter-method'))
        .trigger('search-filter-change');
    });

    // Listen for filter change, provide correct filter
    $iconSet.on('search-filter-change', function(){
      var thisFilter = $(this).attr('current-filter');
      $filterWrap.html(reference.filterMarkup[thisFilter]);
      handlers[thisFilter]();
    });
  }
})(jQuery);

$(document).ready(function() {
  var geocoder = new google.maps.Geocoder();

  function bindNameSearch(hosts){
    $('#name-search').on('input propertychange paste', function(){
      handleNameSearch($(this).children('input').val(), hosts);
    });
  }

  function handleNameSearch(str, hosts){
    if (str == "") {
      return false;
    } else {
      var newMarkup = '',
      count = 0;

      for (var i = 0; i < hosts.length; i++) {
        if (hosts[i].company.name.match(new RegExp(str.toLowerCase(), 'i'))) {
          count ++;
          newMarkup += '<li data-hid="' + hosts[i]._id + '">';
          newMarkup += '<h3 class="view-host-avail">' + hosts[i].company.name + '</h3>';
          newMarkup += '<span class="host-info">$' + hosts[i].company.fee + ' | ' + hosts[i].company.city + ', ' + hosts[i].company.state + '</span>';
          newMarkup += '</li>';
        }
      }

      if (count < 1) {
        newMarkup = '<li>No results for that search</li>';
      }

      $('#resultsWrap').html(newMarkup);
      if(!$('#resultsWrap').hasClass('has-content')){
        $('#resultsWrap').addClass('has-content')
      }
      calendarMdl.bindSideHostAvail({avail: serverData.avails}, function(args1, args2){
          calendarMdl.changeMonth(args1);
          calendarMdl.processSidebarView(args2);
      });
    }
  }

  function bindFeeSearch(hosts){
    var $slider = $('#fee-search input').slider(),
      range = $('#fee-search .tooltip-inner').html().split(' : ');

    range[0] = parseInt(range[0]);
    range[1] = parseInt(range[1]);
    handleFeeSearch(range, hosts);

    $slider.on('slideStop', function(){
      var range = $('#fee-search .tooltip-inner').html().split(' : ');
      range[0] = parseInt(range[0]);
      range[1] = parseInt(range[1]);
      handleFeeSearch(range, hosts);
    });
  }

  function handleFeeSearch(range, hosts) {
    var newMarkup = '',
    count = 0;

    for (var i = 0; i < hosts.length; i++) {
      var inRange = hosts[i].company.fee >= range[0] && hosts[i].company.fee <= range[1];
      if (inRange) {
        count++;
        newMarkup += '<li data-hid="' + hosts[i]._id + '">';
        newMarkup += '<h3 class="view-host-avail">' + hosts[i].company.name + '</h3>';
        newMarkup += '<span class="host-info">$' + hosts[i].company.fee + ' | ' + hosts[i].company.city + ', ' + hosts[i].company.state + '</span>';
        newMarkup += '</li>';
      }
    }
    if (count < 1) {
      newMarkup = '<li>No results for that search</li>';
    }
    $('#resultsWrap').html(newMarkup);
    if(!$('#resultsWrap').hasClass('has-content')){
      $('#resultsWrap').addClass('has-content')
    }
    calendarMdl.bindSideHostAvail({avail: serverData.avails}, function(args1, args2){
        calendarMdl.changeMonth(args1);
        calendarMdl.processSidebarView(args2);
    });
  }

  function bindSizeSearch(hosts){
    $('#size-search select').change(function(e){
      e.preventDefault;
      var sizeCat = parseInt($(this).children('option:selected').val());
      handleSizeSearch(sizeCat, hosts);
    });
  }

  function handleSizeSearch(range, hosts) {
    var newMarkup = '',
    count = 0;
    for (var i = 0; i < hosts.length; i++) {
      if (hosts[i].company.size === range) {
        count++;
        newMarkup += '<li data-hid="' + hosts[i]._id + '">';
        newMarkup += '<h3 class="view-host-avail">' + hosts[i].company.name + '</h3>';
        newMarkup += '<span class="host-info">$' + hosts[i].company.fee + ' | ' + hosts[i].company.city + ', ' + hosts[i].company.state + '</span>';
        newMarkup += '</li>';
      }
    }
    if (count < 1) {
      newMarkup = '<li>No results for that search</li>';
    }
    $('#resultsWrap').html(newMarkup);
    if(!$('#resultsWrap').hasClass('has-content')){
      $('#resultsWrap').addClass('has-content')
    }
    calendarMdl.bindSideHostAvail({avail: serverData.avails}, function(args1, args2){
        calendarMdl.changeMonth(args1);
        calendarMdl.processSidebarView(args2);
    });
  }

  function bindZipSearch(hosts){
    var $zipWrap = $('#zip-search');

    $(document).on('request-hosts', function(){
      console.log(hosts);
    });

    $zipWrap.find('span.bttn.location')
      .unbind('click')
      .click(function(e){
        e.preventDefault();
        e.stopPropagation();
        handleZipSearch($zipWrap.children('input').val(), hosts);
    });
  }

  function handleZipSearch(zip, hosts){
    var lat, lng;
    geocoder.geocode({address: zip}, function(results, status){
      var newMarkup = '',
      count = 0;

      if (status == google.maps.GeocoderStatus.OK){

          var here = {
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng()
          };

          for (var j = 0; j < hosts.length; j++) {
            hosts[j].company.distance = reference.distance(
              hosts[j].company.lat,
              hosts[j].company.lng,
              here.lat,
              here.lng
            );
          }

          hosts.sort(reference.compareDistance);

          for (var j = 0; j < hosts.length; j++) {

            count ++;
            newMarkup += '<li data-hid="' + hosts[j]._id + '">';
            newMarkup += '<h3 class="view-host-avail">' + hosts[j].company.name + '</h3>';
            newMarkup += '<span class="host-info">' + ( hosts[j].company.distance > 1 ? Math.round(hosts[j].company.distance) : hosts[j].company.distance.toFixed(1)) + ' miles | ' + hosts[j].company.city + ', ' + hosts[j].company.state + '</span>';
            newMarkup += '</li>';
          }

          if (count < 1) {
            newMarkup = '<li>No results for that search</li>';
          }

          $('#resultsWrap').html(newMarkup);
          if(!$('#resultsWrap').hasClass('has-content')){
            $('#resultsWrap').addClass('has-content')
          }
          calendarMdl.bindSideHostAvail({avail: serverData.avails}, function(args1, args2){
              calendarMdl.changeMonth(args1);
              calendarMdl.processSidebarView(args2);
          });

      } else {
        console.log('Geocode was not successful: ' + status);
      }
    });
  }

  // Execution
  vs.api.host.getAll(function(hosts){
    var locations = [];
    for (var i = 0; i < hosts.length; i++) {

      (function(i, hosts){

        hosts[i].company.address = hosts[i].company.address + ', '
          + hosts[i].company.city + ', '
          + hosts[i].company.state + ' '
          + hosts[i].company.zip;

        geocoder.geocode({
          address: hosts[i].company.address
        },function(results, status){
          if (status == google.maps.GeocoderStatus.OK){

            locations.push([results[0].geometry.location.lat(), results[0].geometry.location.lng()]);
            hosts[i].company.lat = results[0].geometry.location.lat();
            hosts[i].company.lng = results[0].geometry.location.lng();

            if (hosts.length === locations.length){
              $('#iconWrap').searchFilterIcons({
                name: function(){
                  bindNameSearch(hosts)
                },
                fee: function(){
                  bindFeeSearch(hosts)
                },
                zip: function(){
                  bindZipSearch(hosts)
                },
                size: function(){
                  bindSizeSearch(hosts)
                }
              });
              console.log(hosts);
            }
          } else {
            console.log('Geocode was not successful: ' + status);
          }
        });
      })(i, hosts);
    }
  });
});
