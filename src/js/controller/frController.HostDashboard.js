$(document).ready(function() {
    var availableDayMarkup, $calendarCell, currentHID;
    var initArgs = {
        situ : 'pageload-host'
    };
    calendarMdl.processCalendarView(initArgs);

    $('.changeMonth').click(function(){
        var dir = $(this).attr('data-direction');
        var changeMonthArgs = {
          month : serverData.currentMonth,
          year : serverData.currentYear,
          dir : dir,
          situ : 'pageload-avail',
          plus: false
        };
        calendarMdl.changeMonthArrow(changeMonthArgs);
    });

    $('#selectedHost').change(function(){
        $('.calendarWrap').html(serverData.calendarString);
        var selectHostArgs = {
            avail : serverData.avails,
            situ : 'pageload-avail'
        };
        calendarMdl.processCalendarView(selectHostArgs);
    });
});