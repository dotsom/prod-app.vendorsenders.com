$(document).ready(function() {
    var availableDayMarkup, $calendarCell, currentHID;
    var initArgs = {
      avail : serverData.avails,
      booking : serverData.bookings,
      situ : 'pageload-user'
    };
    calendarMdl.processCalendarView(initArgs);

    $('.changeMonth').click(function(){
        var dir = $(this).attr('data-direction');
        var changeMonthArgs = {
          month : serverData.currentMonth,
          year : serverData.currentYear,
          dir : dir,
          situ : 'pageload-user',
          plus : false
        };
        calendarMdl.changeMonthArrow(changeMonthArgs);
    });
});