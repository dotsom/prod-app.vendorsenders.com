$(document).ready(function() {

  //Bindings

  function bindModalEvents(user, infotype){
    $('#exit-modal').click(function(){
      $('.modal-overlay').remove();
    });

    $('form.edit-ind-form-info').unbind('submit');

    $('form.edit-ind-form-info').submit(function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      handleModalForm(user, infotype);
    });

    switch (infotype) {
      case "firstName":
        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            firstName: {
              validators: {
                notEmpty: {
                  message: 'You must provide a new first name'
                }
              }
            }
          }
        });
        break;

      case "lastName":
        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            lastName: {
              validators: {
                notEmpty: {
                  message: 'You must provide a new last name'
                }
              }
            }
          }
        });
        break;

      case "phone":
        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            phone: {
              validators: {
                phone: {
                  message: 'You must provide a valid phone number'
                }
              }
            }
          }
        });
        break;

      case "email":
        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            email: {
              validators: {
                emailAddress: {
                  message: 'You must provide a valid email address'
                }
              }
            }
          }
        });
        break;

      case "password":
        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            password: {
              validators: {
                notEmpty: {
                  message: 'A new password is required and cannot be empty'
                },
                stringLength: {
                  min: 8,
                  message: 'Your new password must be at least 8 characters'
                }
              }
            },
            cpassword: {
              validators: {
                notEmpty: {
                  message: 'You must confirm the new password'
                },
                identical: {
                  field: 'password',
                  message: 'Your passwords do not match'
                }
              }
            },
          }
        });
        break;

      case "binfo":
        var initVal = $('.edit-ind-form-info').attr('radio-value');
        $('.edit-ind-form-info label input[value=' + initVal + ']').prop('checked', true);

        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            bname: {
              validators: {
                notEmpty: {
                  message: 'You must provide a new business name'
                }
              }
            },
            binfo: {
              validators: {
                notEmpty: {
                  message: 'You must provide a new business description'
                }
              }
            }
          }
        });
        break;

      case "btype":
        var initVal = $('.edit-ind-form-info').attr('radio-value');
        $('.edit-ind-form-info label input[value=' + initVal + ']').prop('checked', true);

        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            btype: {
              validators: {
                notEmpty: {
                  message: 'You must specify what is being sold'
                }
              }
            }
          }
        });
        break;
    }
  }

  function bindUserExitX($e){
    var $thisx = $e.find('.exit-x'),
        thisUser = JSON.parse($e.attr('user')),
        newMarkup = createSingleUserListItem(thisUser);
    $thisx.click(function(e){
      e.stopImmediatePropagation();
      $thisx.parent()
        .html(newMarkup)
        .removeClass('individual-user');
      bindIndUserList($e);
    });
  }

  function bindIndUserList($e){
    $e.click(function(e){
      e.stopPropagation();
      var thisUser = getClickedUser($e),
        thisMarkup = createIndUserList(thisUser);

      $e.html(thisMarkup)
        .unbind('click')
        .addClass('individual-user')
        .find('button')
        .click(function(e){
          e.preventDefault();
          console.log($(this).attr('info-type'));
          var infotype = $(this).attr('info-type');
          displayModal(thisUser, infotype);
        });
      bindUserExitX($(this));
    });
  }

  //Create Markup

  function createModalMarkup(user, infotype){
    console.log(user);

    //Make sure about the business object
    user = setBusFallbacks(user);

    var markup = {
      firstName: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Edit First Name</h4>\
          <form uid="'
          + user._id
          + '" class="edit-ind-form-info">\
            <div class="form-group">\
              <input placeholder="First Name" type="text" class="form-control" id="firstName" name="firstName" autocomplete="off" value="'
              + user.local.firstName
              + '">\
            </div>\
            <div class="form-group">\
                  <button type="submit" class="edit-user-submit bttn bttn-primary" id="editUserSubmit"><span class="fa fa-male"></span> Submit</button>\
                  <span id="modalMessage" class=""></span>\
            </div>\
          </form>\
        </div>\
      </div>',
      lastName: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Edit Last Name</h4>\
          <form uid="'
          + user._id
          + '" class="edit-ind-form-info">\
            <div class="form-group">\
              <input placeholder="Last Name" type="text" class="form-control" id="lastName" name="lastName" autocomplete="off" value="'
              + user.local.lastName
              + '">\
            </div>\
            <div class="form-group">\
                  <button type="submit" class="edit-user-submit bttn bttn-primary" id="editUserSubmit"><span class="fa fa-male"></span> Submit</button>\
                  <span id="modalMessage" class="hidden"></span>\
            </div>\
          </form>\
        </div>\
      </div>',
      email: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Edit Email Address</h4>\
          <form uid="'
          + user._id
          + '" class="edit-ind-form-info">\
            <div class="form-group">\
              <input placeholder="Email Address" type="email" class="form-control" id="email" name="email" autocomplete="off" value="'
              + user.local.email
              + '">\
            </div>\
            <div class="form-group">\
                  <button type="submit" class="edit-user-submit bttn bttn-primary" id="editUserSubmit"><span class="fa fa-male"></span> Submit</button>\
                  <span id="modalMessage" class=""></span>\
            </div>\
          </form>\
        </div>\
      </div>',
      phone: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Edit Primary Phone</h4>\
          <form uid="'
          + user._id
          + '" class="edit-ind-form-info">\
            <div class="form-group">\
              <input placeholder="Primary Phone" type="tel" class="form-control" id="phone" name="phone" autocomplete="off" value="'
              + user.local.phone
              + '">\
            </div>\
            <div class="form-group">\
                  <button type="submit" class="edit-user-submit bttn bttn-primary" id="editUserSubmit"><span class="fa fa-male"></span> Submit</button>\
                  <span id="modalMessage" class=""></span>\
            </div>\
          </form>\
        </div>\
      </div>',
      accountType1: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Edit Primary Phone</h4>\
          <form uid="'
          + user._id
          + '" class="edit-ind-form-info">\
            <div class="form-group">\
              <select class="form-control" id="accountType" name="accountType">\
                <option selected="selected" value="1">Merchant</option>\
                <option value="99">Administrator</option>\
              </select>\
            </div>\
            <div class="form-group">\
              <button type="submit" class="edit-user-submit bttn bttn-primary" id="editUserSubmit"><span class="fa fa-male"></span> Submit</button>\
              <span id="modalMessage" class=""></span>\
            </div>\
          </form>\
        </div>\
      </div>',
      accountType99: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Edit Primary Phone</h4>\
          <form uid="'
          + user._id
          + '" class="edit-ind-form-info">\
            <div class="form-group">\
              <select class="form-control" id="accountType" name="accountType">\
                <option value="1">Merchant</option>\
                <option selected="selected" value="99">Administrator</option>\
              </select>\
            </div>\
            <div class="form-group">\
              <button type="submit" class="edit-user-submit bttn bttn-primary" id="editUserSubmit"><span class="fa fa-male"></span> Submit</button>\
              <span id="modalMessage" class=""></span>\
            </div>\
          </form>\
        </div>\
      </div>',
      password: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Edit Primary Phone</h4>\
          <form class="edit-ind-form-info">\
            <div class="form-group">\
              <div class="form-group">\
                <input placeholder="New Password" type="password" class="form-control" id="password" name="password" autocomplete="off">\
              </div>\
              <div class="form-group">\
                <input placeholder="Confirm New Password" type="password" class="form-control" id="cpassword" name="cpassword" autocomplete="off">\
              </div>\
              <div class="form-group">\
                <button type="submit" class="edit-user-submit bttn bttn-primary" id="editUserSubmit"><span class="fa fa-male"></span> Submit</button>\
                <span id="modalMessage" class=""></span>\
              </div>\
            </div>\
          </form>\
        </div>\
      </div>',
      binfo: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Edit Primary Phone</h4>\
          <form class="edit-ind-form-info">\
            <div class="form-group">\
              <input value="'
              + user.bus.name
              +'" placeholder="Business Name" type="text" class="form-control" id="bname" name="bname" autocomplete="off">\
            </div>\
            <div class="form-group">\
              <textarea placeholder="Brief business description- 3 or 4 sentances to describe the products you may be selling." class="form-control" id="binfo" name="binfo">'
              + user.bus.info
              + '</textarea>\
            </div>\
            <div class="form-group">\
              <button type="submit" class="edit-user-submit bttn bttn-primary" id="editUserSubmit"><span class="fa fa-male"></span> Submit</button>\
              <span id="modalMessage" class=""></span>\
            </div>\
          </form>\
        </div>\
      </div>',
      btype: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Edit Primary Phone</h4>\
          <form radio-value="'
          + user.bus.type
          + '" class="edit-ind-form-info">\
            <div class="modal-radio-group form-group">\
              <label for="btype" class="control-label">Which of these most closely describes what you sell?</label>\
              <label class="radio inline">\
                  <input type="radio" value="1" name="btype" autocomplete="off">\
                  Home Made Food\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="2" name="btype" autocomplete="off">\
                  Packaged Food\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="3" name="btype" autocomplete="off">\
                  Honey, Candy, and Nuts\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="4" name="btype" autocomplete="off">\
                  Jewelry\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="5" name="btype" autocomplete="off">\
                  Candles\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="6" name="btype" autocomplete="off">\
                  Clothing\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="7" name="btype" autocomplete="off">\
                  Cosmetics\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="8" name="btype" autocomplete="off">\
                  Soaps\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="9" name="btype" autocomplete="off">\
                  Nursery\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="10" name="btype" autocomplete="off">\
                  Flowers\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="11" name="btype" autocomplete="off">\
                  Home Goods\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="12" name="btype" autocomplete="off">\
                  Photography\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="13" name="btype" autocomplete="off">\
                  Gardening\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="14" name="btype" autocomplete="off">\
                  Hand Crafted\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="15" name="btype" autocomplete="off">\
                  Marketing\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="17" name="btype" autocomplete="off">\
                  Fashion Accessories\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="18" name="btype" autocomplete="off">\
                  Fashion Accessories\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="19" name="btype" autocomplete="off">\
                  Licenced Items\
              </label>\
              <label class="radio inline">\
                  <input type="radio" value="19" name="btype" autocomplete="off">\
                  <input placeholder="Other" type="text" class="form-control" id="btype-oth" name="btype-oth" autocomplete="off">\
              </label>\
            </div>\
            <div class="form-group">\
              <button type="submit" class="edit-user-submit bttn bttn-primary" id="editUserSubmit"><span class="fa fa-male"></span> Submit</button>\
              <span id="modalMessage" class=""></span>\
            </div>\
          </form>\
        </div>\
      </div>'
    };

    if (infotype == 'accountType'){
      infotype = 'accountType' + user.local.accountType;
    }

    return markup[infotype];
  }

  function createIndUserList(user){
    console.log(user);
    var readableAcctStatus, readableAcctType;

    switch (user.local.accountType) {
      case 1:
        readableAcctType = 'Merchant';
        break;
      case 2:
        readableAcctType = "Host Company";
        break;
      case 99:
        readableAcctType = "Administrator";
        break;
      default:
        readableAcctType = "Unknown";
        break;
    }

    user = setBusFallbacks(user);

    var markup = '<span id="';
    markup += 'close-' + user._id;
    markup += '" class="exit-x fa fa-times"></span><ul><li class="user-title"><span class="col-sm-12"><h3>';
    markup += 'Edit User: ' + user.local.firstName + ' ' + user.local.lastName;
    markup += '</h3><a href="'
    markup += '/admin/user-calendar/' + user._id;
    markup += '" info-type="firstName" class="bttn bttn-primary"><span class="fa fa-calendar"></span> View Calendar</a></span></li>';
    markup += '<li class="user-attr" id="change-firstName"><span class="space col-sm-1"></span><span class="user-label col-sm-7">First Name: ';
    markup += user.local.firstName;
    markup += '</span><span class="align-right col-sm-4"><button info-type="firstName" class="bttn bttn-primary"><span class="fa fa-pencil"></span> Edit</button></span></li>';
    markup += '<li class="user-attr" id="change-lastName"><span class="space col-sm-1"></span><span class="user-label col-sm-7">Last Name: ';
    markup += user.local.lastName;
    markup +='</span><span class="align-right col-sm-4"><button info-type="lastName" class="bttn bttn-primary"><span class="fa fa-pencil"></span> Edit</button></span></li>';
    markup += '<li class="user-attr" id="change-email"><span class="space col-sm-1"></span><span class="user-label col-sm-7">Email: ';
    markup += user.local.email;
    markup += '</span><span class="align-right col-sm-4"><button info-type="email" class="bttn bttn-primary"><span class="fa fa-pencil"></span> Edit</button></span></li>';
    markup += '<li class="user-attr" id="change-phone"><span class="space col-sm-1"></span><span class="user-label col-sm-7">Phone: ';
    markup += user.local.phone;
    markup += '</span><span class="align-right col-sm-4"><button info-type="phone" class="bttn bttn-primary"><span class="fa fa-pencil"></span> Edit</button></span></li>';
    markup += '<li class="user-attr" id="change-accounType"><span class="space col-sm-1"></span><span class="user-label col-sm-7">Account Type: ';
    markup += readableAcctType;
    markup += '</span><span class="align-right col-sm-4"><button info-type="accountType" class="bttn bttn-primary"><span class="fa fa-pencil"></span> Edit</button></span></li>';
    markup += '<li class="user-attr" id="change-password"><span class="space col-sm-1"></span><span class="user-label col-sm-7">Password: *********</span><span class="align-right col-sm-4"><button info-type="password" class="bttn bttn-primary"><span class="fa fa-pencil"></span> Edit</button></span></li>';
    markup += '<li class="user-attr" id="change-binfo"><span class="space col-sm-1"></span><span class="user-label col-sm-7">Business Title and Information</span><span class="align-right col-sm-4"><button info-type="binfo" class="bttn bttn-primary"><span class="fa fa-pencil"></span> Edit</button></span></li>';
    markup += '<li class="user-attr" id="change-btype"><span class="space col-sm-1"></span><span class="user-label col-sm-7">Business Type: ';
    markup += user.bus.type ? reference.btypes[user.bus.type] : 'Hasn\'t Specified';
    markup += '</span><span class="align-right col-sm-4"><button info-type="btype" class="bttn bttn-primary"><span class="fa fa-pencil"></span> Edit</button></span></li>';
    markup += '</ul>';

    return markup;
  }

  function createUserTable(users){
    var i = 1;
    var markup = '<hr/><ul class="user-list">';
    _.each(users, function(user){
      if(user.local.accountType != 2){
        var thisUserJson = JSON.stringify(user);
        markup += '<li data-uid="';
        markup += user._id;
        markup += '" class="';
        if (i % 2 == 0) {
          markup += 'light" user=\'' + thisUserJson + '\'><span class="col-sm-3 username">';
        } else {
          markup += 'dark" user=\'' + thisUserJson + '\'><span class="col-sm-3 username">';
        }
        markup += user.local.firstName + ' ' + user.local.lastName;
        markup += '</span><span class="col-sm-7 email">' + user.local.email + '</span>';
        markup += '<span class="col-sm-2 edit"><span class="edit-pencil show-on-hover fa fa-pencil"></span>Edit</span>';
        markup += '</li>';
        i++;
      }
    });
    markup += '</ul>';

    return markup;
  }

  function createSingleUserListItem(user){
    var markup = '<span class="col-sm-3 username">';
    markup += user.local.firstName + ' ' + user.local.lastName;
    markup += '</span><span class="col-sm-7 email">' + user.local.email + '</span>';
    markup += '<span class="col-sm-2 edit"><span class="edit-pencil show-on-hover fa fa-pencil"></span>Edit</span>';

    return markup;
  }

  function displayModal(user, infotype){
    var thisMarkup = createModalMarkup(user, infotype);
    $('body').prepend(thisMarkup);
    bindModalEvents(user, infotype);
  }

  function getClickedUser(e){
    var thisUser = $(e).attr('user');
    thisUser = JSON.parse(thisUser);
    return thisUser;
  }

  function handleModalForm(user, infotype){
    console.log(user, infotype);
    var data = {},
      urlString = '/api/users/' + user._id;

    switch (infotype) {
      case "firstName":
        data.firstName = $('#firstName').val();
        break;

      case "lastName":
        data.lastName = $('#lastName').val();
        break;

      case "phone":
        data.phone = $('#phone').val();
        break;

      case "email":
        data.email = $('#email').val();
        break;

      case "accountType":
        data.accountType = $('#accountType').val();
        break;

      case "password":
        data.password = $('#password').val();
        break;

      case "binfo":
        data.bname = $('#bname').val();
        data.binfo = $('#binfo').val();
        break;

      case "btype":
        data.btype = $('input[name=btype]:checked').val();
        break;
    }

    // SEND POST REQUEST WITH DATA OBJECT
    $.ajax({
        type : 'PUT',
        url : urlString,
        data : data,
        dataType : 'json',
        encode : true
    })

    // RECEIVE AND DELEGATE TO FUNCTION
    .done(function(data) {
        console.log(data);
        $('#modalMessage').html(data.message).addClass('success shown');
        initUserMgmt();
    });
  }

  function setBusFallbacks(user){
    if (typeof user.bus != "undefined") {
      if (typeof user.bus.name == "undefined") {
        user.bus.name = '';
      }
      if (typeof user.bus.info == "undefined") {
        user.bus.info = '';
      }
      if (typeof user.bus.type == "undefined") {
        user.bus.type = '';
      }
    } else {
      user.bus = {
        name: '',
        info: '',
        type: ''
      };
    }

    return user;
  }

  function initUserMgmt(){
    UserAPI.getAll(function(users){

      //Create User Table
      var tableMarkup = createUserTable(users);
      $('#admin-user-table').html(tableMarkup);

      //Bind click events
      $('.user-list li').click(function(){
        var thisUser = getClickedUser(this);
        var markup = createIndUserList(thisUser);
        $(this).html(markup)
          .unbind('click')
          .addClass('individual-user')
          .find('button')
          .click(function(e){
            e.preventDefault();
            console.log($(this));
            var infotype = $(this).attr('info-type');
            displayModal(thisUser, infotype);
          });
        bindUserExitX($(this));
      });

      //Open User if specified in params
      if (query) {
        $('.user-list li[data-uid=' + query + ']').trigger('click');
      }
    });
  }

  initUserMgmt();
});