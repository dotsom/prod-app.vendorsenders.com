$(document).ready(function() {

  //Bindings

  function bindModalEvents(host, infotype){
    $('#exit-modal').click(function(){
      $('.modal-overlay').remove();
    });

    $('form.edit-ind-form-info').unbind('submit');
    var submitOnce = 0;
    var $form = $('form.edit-ind-form-info');

    if (!$form.hasClass('default-behavior')) {
      $form.submit(function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        handleModalForm(host, infotype);
      });
    }

    switch (infotype) {
      case "name":
        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This name is not valid',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            firstName: {
              validators: {
                notEmpty: {
                  message: 'You must provide a new first name'
                }
              }
            }
          }
        });
        break;

      case "phone":
        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This phone number is not valid',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            phone: {
              validators: {
                phone: {
                  message: 'You must provide a valid phone number'
                }
              }
            }
          }
        });
        break;

      case "email":
        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This email is not valid',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            email: {
              validators: {
                emailAddress: {
                  message: 'You must provide a valid email address'
                }
              }
            }
          }
        });
        break;

      case "password":
        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            password: {
              validators: {
                notEmpty: {
                  message: 'A new password is required and cannot be empty'
                },
                stringLength: {
                  min: 8,
                  message: 'Your new password must be at least 8 characters'
                }
              }
            },
            cpassword: {
              validators: {
                notEmpty: {
                  message: 'You must confirm the new password'
                },
                identical: {
                  field: 'password',
                  message: 'Your passwords do not match'
                }
              }
            },
          }
        });
        break;

      case "binfo":
        var initVal = $('.edit-ind-form-info').attr('radio-value');
        $('.edit-ind-form-info label input[value=' + initVal + ']').prop('checked', true);

        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            bname: {
              validators: {
                notEmpty: {
                  message: 'You must provide a new business name'
                }
              }
            },
            binfo: {
              validators: {
                notEmpty: {
                  message: 'You must provide a new business description'
                }
              }
            }
          }
        });
        break;

      case "upload":

        $('.edit-ind-form-info').bootstrapValidator({
          message: 'This filetype is not allowed',
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            btype: {
              validators: {
                file: {
                  extension: 'jpeg,png,gif,pdf',
                  type: 'image/jpeg,image/png,image/gif,application/pdf',
                  maxSize: 8000000,   // 2048 * 1024
                  message: 'The selected file is not valid'
                }
              }
            }
          }
        });
        break;
    }
  }

  function bindHostExitX($e){
    var $thisx = $e.find('.exit-x'),
        thisHost = JSON.parse($e.attr('host')),
        newMarkup = createSingleHostListItem(thisHost);
    $thisx.click(function(e){
      e.stopImmediatePropagation();
      $thisx.parent()
        .html(newMarkup)
        .removeClass('individual-host');
      bindIndHostList($e);
    });
  }

  function bindIndHostList($e){
    $e.click(function(e){
      e.stopPropagation();
      var thisHost = getClickedHost($e),
        thisMarkup = createIndHostList(thisHost);

      $e.html(thisMarkup)
        .unbind('click')
        .addClass('individual-host')
        .find('button')
        .click(function(e){
          e.preventDefault();
          var infotype = $(this).attr('info-type');
          displayModal(thisHost, infotype);
        });
      bindHostExitX($(this));
    });
  }

  //Create Markup

  function createModalMarkup(host, infotype){
    var markup = {
      email: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Edit Email Address</h4>\
          <form hid="'
          + host._id
          + '" class="edit-ind-form-info">\
            <div class="form-group">\
              <input placeholder="Email Address" type="email" class="form-control" id="email" name="email" autocomplete="off" value="'
              + host.profile.email
              + '">\
            </div>\
            <div class="form-group">\
                  <button type="submit" class="edit-host-submit bttn bttn-primary" id="editHostSubmit"><span class="fa fa-male"></span> Submit</button>\
                  <span id="modalMessage" class=""></span>\
            </div>\
          </form>\
        </div>\
      </div>',
      phone: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Edit Phone Number</h4>\
          <form hid="'
          + host._id
          + '" class="edit-ind-form-info">\
            <div class="form-group">\
              <input placeholder="Phone" type="tel" class="form-control" id="phone" name="phone" autocomplete="off" value="'
              + host.company.phone
              + '">\
            </div>\
            <div class="form-group">\
                  <button type="submit" class="edit-host-submit bttn bttn-primary" id="editHostSubmit"><span class="fa fa-male"></span> Submit</button>\
                  <span id="modalMessage" class=""></span>\
            </div>\
          </form>\
        </div>\
      </div>',
      password: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Change Password</h4>\
          <form class="edit-ind-form-info">\
            <div class="form-group">\
              <div class="form-group">\
                <input placeholder="New Password" type="password" class="form-control" id="password" name="password" autocomplete="off">\
              </div>\
              <div class="form-group">\
                <input placeholder="Confirm New Password" type="password" class="form-control" id="cpassword" name="cpassword" autocomplete="off">\
              </div>\
              <div class="form-group">\
                <button type="submit" class="edit-host-submit bttn bttn-primary" id="editHostSubmit"><span class="fa fa-male"></span> Submit</button>\
                <span id="modalMessage" class=""></span>\
              </div>\
            </div>\
          </form>\
        </div>\
      </div>',
      upload: '<div class="modal-overlay">\
        <div class="well col-sm-4 modal-box">\
          <span id="exit-modal" class="exit-x fa fa-times"></span>\
          <h4>Manage Uploads</h4>\
          <ul class="file-uploads"></ul>\
          <form action="/api/files/' + host._id + '" method="post" enctype="multipart/form-data" class="default-behavior edit-ind-form-info">\
            <div class="form-group">\
              <input type="text" placeholder="File Name (No Special Characters)" class="form-control" id="name" name="name" autocomplete="off">\
              <input type="hidden" value="' + host._id + '" class="form-control" id="hid" name="hid" autocomplete="off">\
            </div>\
            <div class="form-group">\
              <input type="file" class="form-control" id="upload" name="upload" autocomplete="off">\
            </div>\
            <div class="form-group">\
              <button type="submit" class="edit-host-submit bttn bttn-primary" id="editHostSubmit"><span class="fa fa-male"></span> Submit</button>\
              <span id="modalMessage" class=""></span>\
            </div>\
          </form>\
          <ul class="file-list"></ul>\
        </div>\
      </div>'
    };

    return markup[infotype];
  }

  function createIndHostList(host){
    var readableAcctStatus,
      readableAcctType = "Host Company";

    var markup = '<span id="';
    markup += 'close-' + host._id;
    markup += '" class="exit-x fa fa-times"></span><ul><li class="host-title"><span class="col-sm-12"><h3>';
    markup += 'Edit Host: ' + host.company.name;
    markup += '</h3><a href="'
    markup += '/admin/host-calendar/' + host._id;
    markup += '" class="bttn bttn-primary"><span class="fa fa-calendar"></span> View Calendar</a></span></li>';
    markup += '<li class="host-attr" id="change-email"><span class="space col-sm-1"></span><span class="host-label col-sm-7">Email: ';
    markup += host.profile.email;
    markup += '</span><span class="align-right col-sm-4"><button info-type="email" class="bttn bttn-primary"><span class="fa fa-pencil"></span> Edit</button></span></li>';
    markup += '<li class="host-attr" id="change-phone"><span class="space col-sm-1"></span><span class="host-label col-sm-7">Phone: ';
    markup += host.company.phone;
    markup += '</span><span class="align-right col-sm-4"><button info-type="phone" class="bttn bttn-primary"><span class="fa fa-pencil"></span> Edit</button></span></li>';
    markup += '<li class="host-attr" id="change-password"><span class="space col-sm-1"></span><span class="host-label col-sm-7">Password: *********</span><span class="align-right col-sm-4"><button info-type="password" class="bttn bttn-primary"><span class="fa fa-pencil"></span> Edit</button></span></li>';
    markup += '<li class="host-attr" id="upload-files"><span class="space col-sm-1"></span><span class="host-label col-sm-7">Manage Uploads</span><span class="align-right col-sm-4"><button info-type="upload" class="bttn bttn-primary"><span class="fa fa-cloud-upload"></span> Go!</button></span></li>';
    markup += '</ul>';

    return markup;
  }

  function createHostTable(hosts){
    var i = 1;
    var markup = '<hr/><ul class="host-list">';
    _.each(hosts, function(host){
      var thisHostJson = JSON.stringify(host);
      markup += '<li data-hid="';
      markup += host._id;
      markup += '" class="';
      if (i % 2 == 0) {
        markup += 'light" host=\'' + thisHostJson + '\'><span class="col-sm-3 hostname">';
      } else {
        markup += 'dark" host=\'' + thisHostJson + '\'><span class="col-sm-3 hostname">';
      }
      markup += host.company.name;
      markup += '</span><span class="col-sm-7 email">' + host.profile.email + '</span>';
      markup += '<span class="col-sm-2 edit"><span class="edit-pencil show-on-hover fa fa-pencil"></span>Edit</span>';
      markup += '</li>';
      i++;
    });
    markup += '</ul>';

    return markup;
  }

  function createSingleHostListItem(host){
    var markup = '<span class="col-sm-3 hostname">';
      markup += host.company.name;
      markup += '</span><span class="col-sm-7 email">' + host.profile.email + '</span>';
      markup += '<span class="col-sm-2 edit"><span class="edit-pencil show-on-hover fa fa-pencil"></span>Edit</span>';

    return markup;
  }

  function displayFileList(host){
    vs.api.file.getByUid(host._id, function(files){
      var markup = "";
      for (var i = 0; i < files.length; i++) {
        markup += '<li class="file-list-item">';
        markup += '<a target="_blank" href="' + files[i].path + '">' + files[i].name + '</a>';
        markup += '</li>';
      }
      $('.file-list').html(markup);
    });
  }

  function displayModal(host, infotype){
    console.log(host, infotype);
    var thisMarkup = createModalMarkup(host, infotype);
    $('body').prepend(thisMarkup);
    if (infotype == "upload") {
      displayFileList(host);
    }
    bindModalEvents(host, infotype);
  }

  function getClickedHost(e){
    var thisHost = $(e).attr('host');
    thisHost = JSON.parse(thisHost);
    return thisHost;
  }

  function handleModalForm(host, infotype){
    var data = {},
      urlString = '/api/hosts/' + host._id;

    switch (infotype) {
      case "phone":
        data.company = {phone: $('#phone').val().replace(/\D/g,'')};
        break;

      case "email":
        data.profile = {email: $('#email').val()};
        break;

      case "password":
        data.profile = {password: $('#password').val()};
        break;

      case "upload":
        console.log($('#upload')[0].files[0]);
        break;
    }

    // SEND POST REQUEST WITH DATA OBJECT
    $.ajax({
        type : 'PUT',
        url : urlString,
        data : data,
        dataType : 'json',
        encode : true
    })

    // RECEIVE AND DELEGATE TO FUNCTION
    .done(function(data) {
      $('#modalMessage').html(data.message).addClass('primary shown');
      initHostMgmt();
    });
  }

  function setBusFallbacks(host){
    if (typeof host.bus != "undefined") {
      if (typeof host.bus.name == "undefined") {
        host.bus.name = '';
      }
      if (typeof host.bus.info == "undefined") {
        host.bus.info = '';
      }
      if (typeof host.bus.type == "undefined") {
        host.bus.type = '';
      }
    } else {
      host.bus = {
        name: '',
        info: '',
        type: ''
      };
    }

    return host;
  }

  function initHostMgmt(){
    vs.api.host.getAll(function(hosts){

      // Create the table
      var tableMarkup = createHostTable(hosts);
      $('#admin-host-table').html(tableMarkup);

      // Register click event
      $('.host-list li').click(function(){
        var thisHost = getClickedHost(this);
        var markup = createIndHostList(thisHost);
        $(this).html(markup)
          .unbind('click')
          .addClass('individual-host')
          .find('button')
          .click(function(e){
            e.preventDefault();
            var infotype = $(this).attr('info-type');
            displayModal(thisHost, infotype);
          });
        bindHostExitX($(this));
      });

      //Open User if specified in params
      if (query) {
        $('.host-list li[data-hid=' + query + ']').trigger('click');
      }
    });
  }

  initHostMgmt();
});