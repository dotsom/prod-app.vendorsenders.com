$(document).ready(function() {

    // BOOTSTRAP VALIDATION PART 1
    function validateSignup() {
        $('#forgot_pw_form').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        emailAddress: {
                            message: 'This is not a valid email address'
                        }
                    }
                }
            }
        });
    }

    // FORMAT AND SUBMIT THE FIRST PART VIA AJAX
    function submitForm() {
        // DEFINE
        var email = $("input#email").val();

        // BUILD DATA OBJECT
        var data = {
            email:  email
        };

        // SEND POST REQUEST WITH DATA OBJECT
        $.ajax({
            type : 'POST',
            url : '/api/pwtoken',
            data : data,
            dataType : 'json',
            encode : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            console.log(data);
        });
    }

    // CALL THE FIRST VALIDATE ON LOAD
    validateSignup();

    $("form#forgot_pw_form").unbind('submit');
    $("form#forgot_pw_form").submit(function(event) {
        $('#signupButton').attr('disabled', 'disabled');

        // PREVENT FORM FROM DEFAULT SUBMISSION VIA POST
        event.preventDefault();
        event.stopImmediatePropagation();
        submitForm();
        return false;
    });
});