$(document).ready(function() {

    // BOOTSTRAP VALIDATION PART 1
    function validateSignup1() {
        console.log('1');
        $('#signupForm1').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        emailAddress: {
                            message: 'This is not a valid email address'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'A password is required and cannot be empty'
                        },
                        stringLength: {
                            min: 8,
                            message: 'Your password must be at least 8 characters'
                        }
                    }
                },
                cpassword: {
                    validators: {
                        notEmpty: {
                            message: 'You must confirm your password'
                        },
                        identical: {
                            field: 'password',
                            message: 'Your passwords do not match'
                        }
                    }
                },
                firstName: {
                    validators: {
                        notEmpty: {
                            message: 'You must provide a first name'
                        }
                    }
                },
                lastName: {
                    validators: {
                        notEmpty: {
                            message: 'You must provide a last name'
                        }
                    }
                },
                phone: {
                    validators: {
                        phone: {
                            message: 'This is not a valid phone number'
                        }
                    }
                },
            }
        });
    }

   function validateSignup2() {
        console.log('2');
        $('#signupForm2').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                bname: {
                    validators: {
                        notEmpty: {
                            message: 'You must provide a business name'
                        }
                    }
                },
                binfo: {
                    validators: {
                        notEmpty: {
                            message: 'You must provide a description of your business'
                        }
                    }
                },
                btype: {
                    validators: {
                        notEmpty: {
                            message: 'You have to tell us what you sell'
                        }
                    }
                }
            }
        });
    }

    // RETRIEVE THE SECOND PART OF THE FORM AND ANIMATE THE TRANSITION
    function transitionForm1(prevData){
        if (!prevData.signupErrorId) {
            $('#signupWrap').addClass('reloading')
            .delay(700)
            .queue(function(){
                $.ajax({
                    type : 'GET',
                    url : '/signup-2',
                    dataType : 'html',
                }).done(function(data) {
                    $('#signupWrap').html(data).removeClass('reloading');
                    validateSignup2();
                    console.log('we here!');
                    $("form#signupForm2").unbind('submit');
                    $("form#signupForm2").submit(function(event) {

                        $('#signupButton').attr('disabled', 'disabled');

                        // PREVENT FORM FROM DEFAULT SUBMISSION VIA POST
                        event.preventDefault();
                        event.stopImmediatePropagation();
                        submitForm2(prevData.user);
                        return false;
                    });
                });
            });
        } else {
            console.log(data.err);
        }
    }

    // FORMAT AND SUBMIT THE FIRST PART VIA AJAX
    function submitForm1() {
        // DEFINE
        var email = $("input#email").val(),
            password = $("input#password").val(),
            firstName = $("input#firstName").val(),
            lastName = $("input#lastName").val(),
            phone = $("input#phone").val(),
            phoneClean = phone.replace(/\D/g,''),
            accountType = $("input#accountType").val();

        // BUILD DATA OBJECT
        var data = {
            email:  email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            phone: phoneClean,
            accountType: accountType
        };

        // SEND POST REQUEST WITH DATA OBJECT
        $.ajax({
            type : 'POST',
            url : '/signup-basic',
            data : data,
            dataType : 'json',
            encode : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            console.log(data);
            transitionForm1(data);
        });
    }

    // FORMAT AND SUBMIT THE FIRST PART VIA AJAX
    function submitForm2(user) {
        // DEFINE
        var bname = $("input#bname").val(),
            binfo = $("textarea#binfo").val(),
            btype = $("input[name=btype]:checked").val(),
            uid = user._id;

        // BUILD DATA OBJECT
        var data = {
            bname : bname,
            binfo : binfo,
            btype : btype,
            uid : uid
        };

        // SEND POST REQUEST WITH DATA OBJECT
        $.ajax({
            type : 'POST',
            url : '/signup-biz',
            data : data,
            dataType : 'json',
            encode : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            if (!data.signupErrorId) {
                window.location = '/dashboard';
            }
        });
    }

    // CALL THE FIRST VALIDATE ON LOAD
    validateSignup1();

    $("form#signupForm1").unbind('submit');
    $("form#signupForm1").submit(function(event) {

        $('#signupButton').attr('disabled', 'disabled');

        // PREVENT FORM FROM DEFAULT SUBMISSION VIA POST
        event.preventDefault();
        event.stopImmediatePropagation();
        submitForm1();
        return false;
    });
});