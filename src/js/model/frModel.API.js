var vs = {
  api: {
    booking: {
      getAll: function(cb) {
        $.ajax({
          type : 'GET',
          url : '/api/booking',
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getById: function(bid, cb) {
        $.ajax({
          type : 'GET',
          url : '/api/booking/' + bid,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getByUid: function(uid, cb) {
        $.ajax({
          type : 'GET',
          url : '/api/booking/uid/' + uid,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getByAid: function(aid, cb) {
        $.ajax({
          type : 'GET',
          url : '/api/booking/aid/' + aid,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getByHid: function(hid, cb) {
        $.ajax({
          type : 'GET',
          url : '/api/booking/hid/' + hid,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      }
    },

    availability: {
      getAll: function(cb) {
        $.ajax({
          type : 'GET',
          url : '/api/availability',
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getById: function(aid, cb) {
        $.ajax({
          type : 'GET',
          url : '/api/availability/' + aid,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getByYear: function(year, cb) {
        $.ajax({
          type : 'GET',
          url : '/api/availability/year/' + year,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getByMonthAndYear: function(month, year, cb) {
        $.ajax({
          type : 'GET',
          url : '/api/availability/year/' + year + '/month/' + month,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      }
    },

    user: {
      getAll: function(cb) {
        $.ajax({
          type : 'GET',
          url : '/api/users',
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getById: function(uid, cb){

        var reqUrl = '/api/users/' + uid;

        $.ajax({
          type : 'GET',
          url : reqUrl,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      deleteById: function(uid, cb){

        var reqUrl = '/api/users/' + uid;

        $.ajax({
          type : 'DELETE',
          url : reqUrl,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      newUser: function(userData, cb){

        $.ajax({
          type : 'POST',
          url : '/api/users',
          data : userData,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      updateUser: function(userData, cb){

        $.ajax({
          type : 'PUT',
          url : '/api/users',
          data : userData,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },
    },

    host: {
      getAll: function(cb) {
        $.ajax({
          type : 'GET',
          url : '/api/hosts',
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getById: function(hid, cb){
        var reqUrl = '/api/hosts/' + hid;

        $.ajax({
          type : 'GET',
          url : reqUrl,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },
      newHost: function(hostData, cb){

        $.ajax({
          type : 'POST',
          url : '/api/hosts',
          data : hostData,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },
      editHost: function(host, cb){

        $.ajax({
          type : 'PUT',
          url : '/api/hosts/' + host._id,
          data : host,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      }
    },

    notification: {
      getById: function(_id, cb){
        var reqUrl = '/api/notifications/' + _id;

        $.ajax({
          type : 'GET',
          url : reqUrl,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getByUid: function(uid, cb){
        var reqUrl = '/api/notifications/uid/' + uid;

        $.ajax({
          type : 'GET',
          url : reqUrl,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      newNotification: function(newData, cb){

        $.ajax({
          type : 'POST',
          url : '/api/notifications',
          data : newData,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      }
    },

    availability: {
      getAll: function(cb) {
        $.ajax({
          type : 'GET',
          url : '/api/availability',
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getById: function(aid, cb){
        var reqUrl = '/api/availability/' + aid;

        $.ajax({
          type : 'GET',
          url : reqUrl,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      }
    },

    file: {
      getByUid: function(uid, cb) {
        $.ajax({
          type : 'GET',
          url : '/api/files/' + uid,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      }
    },

    waitlist: {
      getAll: function(cb) {
        $.ajax({
          type : 'GET',
          url : '/api/waitlist',
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getById: function(_id, cb){

        var reqUrl = '/api/waitlist/' + _id;

        $.ajax({
          type : 'GET',
          url : reqUrl,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      getByAid: function(aid, cb){

        var reqUrl = '/api/waitlist/aid/' + aid;

        $.ajax({
          type : 'GET',
          url : reqUrl,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      remove: function(_id, cb){

        var reqUrl = '/api/waitlist/' + _id;

        $.ajax({
          type : 'DELETE',
          url : reqUrl,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      create: function(waitlistData, cb){

        $.ajax({
          type : 'POST',
          url : '/api/waitlist',
          data : waitlistData,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      addUser: function(waitlistData, cb){

        $.ajax({
          type : 'PUT',
          url : '/api/waitlist/add-user',
          data : waitlistData,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      },

      removeUser: function(waitlistData, cb){

        $.ajax({
          type : 'PUT',
          url : '/api/waitlist/remove-user',
          data : waitlistData,
          dataType : 'json',
          encode : true
        })

        .done(function(data) {
          if (cb) {
            cb(data);
          }
        });
      }
    }
  }
};