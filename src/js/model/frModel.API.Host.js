var HostAPI = {};

$(document).ready(function() {

  HostAPI.getAll = function(cb) {
    $.ajax({
      type : 'GET',
      url : '/api/hosts',
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  }

  HostAPI.getById = function(hid, cb){
    var reqUrl = '/api/hosts/' + hid;

    $.ajax({
      type : 'GET',
      url : reqUrl,
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  };
});