var calendarMdl = {};
$(document).ready(function() {

    /**
     * The args object can take values for the following keys
     * @param  month
     * @param  year
     * @param  direction
     * @param  situ
     * @param  plus
     */
    calendarMdl.changeMonthArrow = function(args) {
        args = typeof args !== 'undefined' ? args : {};
        args.plus = typeof args.plus !== 'undefined' ? args.plus : true;
        $.ajax({
            type : 'POST',
            url : '/calendar',
            data :  {
                reqType : args.plus ? 'changeMonthArrow' : 'changeMonthArrowNoPlus',
                currentMonth : args.month,
                currentYear : args.year,
                direction : args.dir
            },
            dataType    : 'json',
            encode      : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            var currentCalendarView = $('.calendarWrap').attr('data-calendar-view');

            var newArgs = {
                avail: data.avails,
                booking: serverData.bookings,
                hid: typeof args.hid !== 'undefined' ? args.hid : '',
            };

            serverData.calendarString = data.newCalendar;
            serverData.currentMonth = data.newMonth;
            serverData.currentYear = data.newYear;
            serverData.avails = data.avails;

            switch (currentCalendarView) {
                case "1":
                    newArgs.situ = 'pageload-user';
                break;

                case "2":
                    newArgs.situ = 'pageload-user';
                break;

                case "3":
                    newArgs.situ = 'load-host';
                    newArgs.hid = $('#current-hid').attr('data-hid');
                break;
            }


            $('.calendarWrap').html(data.newCalendar);
            $('#monthName').html(serverData.Calendar.monthNames[serverData.currentMonth] + " " + serverData.currentYear);
            calendarMdl.processCalendarView(newArgs);
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  month
     * @param  year
     * @param  situ
     * @param  plus
     */
    calendarMdl.changeMonth = function(args) {
        args = typeof args !== 'undefined' ? args : {};
        args.plus = typeof args.plus !== 'undefined' ? args.plus : true;
        $.ajax({
            type : 'POST',
            url : '/calendar',
            data :  {
                reqType : args.plus ? 'changeMonth' : 'changeMonthNoPlus',
                currentMonth : args.month,
                currentYear : args.year,
            },
            dataType    : 'json',
            encode      : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            var newArgs = {};
            serverData.calendarString = data.newCalendar;
            serverData.currentMonth = data.newMonth;
            serverData.currentYear = data.newYear;
            serverData.avails = data.avails;
            newArgs.avail = data.avails;
            newArgs.situ = args.situ;
            newArgs.hid = typeof args.hid !== 'undefined' ? args.hid : '';

            $('.calendarWrap').html(data.newCalendar);
            $('#monthName').html(serverData.Calendar.monthNames[serverData.currentMonth] + " " + serverData.currentYear);
            calendarMdl.processCalendarView(newArgs);
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  month
     * @param  year
     * @param  day
     */
    calendarMdl.findAvailability = function(args) {
        cargs = typeof args !== 'undefined' ? args : {};

        var availsToday = [];
        for (var i = 0; i < serverData.avails.length; i++) {
            if (serverData.avails[i].year === args.year && serverData.avails[i].month === args.month && serverData.avails[i].day === args.day) {
                availsToday.push(serverData.avails[i]);
            }
        }
        for (var i = 0; i < availsToday.length; i++) {
            for (var j = 0; j < serverData.hosts.length; j++) {
                if (availsToday[i].hid === serverData.hosts[j]._id) {
                    availsToday[i].hostInfo = serverData.hosts[j];
                }
            }
        }
        return availsToday;
    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindFindAvailability = function (args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};
        if (typeof args.el != 'undefined') {
            el.click(function(){
                var day = parseInt($(this).parent().data('day'));
                var findAvailArgs = {
                    day : day,
                    month: serverData.currentMonth,
                    year : serverData.currentYear
                };
                newArgs.avail = calendarMdl.findAvailability(findAvailArgs);
                newArgs.situ = 'display-avail';
                calendarMdl.processSidebarView(newArgs);
            });
        } else {
            $('.view-avail').click(function(){
                var day = parseInt($(this).parent().parent().data('day'));
                var findAvailArgs = {
                    day : day,
                    month: serverData.currentMonth,
                    year : serverData.currentYear
                };
                newArgs.avail = calendarMdl.findAvailability(findAvailArgs);
                newArgs.situ = 'display-avail';
                newArgs.day = day;
                calendarMdl.processSidebarView(newArgs);
            });
        }
    };

    /**
     * The args object can take values for the following keys
     * @param  month
     * @param  year
     * @param  day
     * @param  hid
     * @param  start
     * @param  end
     */
    calendarMdl.addAvailability = function(args) {
        args = typeof args !== 'undefined' ? args : {};

        $.ajax({
            type        : 'POST',
            url         : '/calendar',
            data        :  {
                reqType : 'addAvail',
                month : args.month,
                year : args.year,
                day : args.day,
                start : args.start,
                end : args.end,
                hid : args.hid
            },
            dataType    : 'json',
            encode      : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            if (data.status == 'success') {
                serverData.avails.push(data.avail);
                var newArgs = {
                    avail : data.avail,
                    situ : 'availAdded'
                };
                calendarMdl.processCalendarView(newArgs);
            }
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindAddAvailability = function(args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};
        if (args.el) {
            $(args.el).click(function(){
                newArgs.year = '' + serverData.currentYear;
                newArgs.month = serverData.currentMonth < 10 ? '0' + serverData.currentMonth : '' + serverData.currentMonth;
                var day = $(this).parent().parent().data('day');
                newArgs.day = day < 10 ? '0' + day : '' + day;
                newArgs.hid = $('#selectedHost').val();
                newArgs.start = $('#startTime').val();
                newArgs.end = $('#endTime').val();

                if (newArgs.hid && newArgs.start && newArgs.end) {
                    calendarMdl.addAvailability(newArgs);
                }
            });
        } else {
            $('.add-booking').click(function(){
                newArgs.year = '' + serverData.currentYear;
                newArgs.month = serverData.currentMonth < 10 ? '0' + serverData.currentMonth : '' + serverData.currentMonth;
                var day = $(this).parent().parent().data('day');
                newArgs.day = day < 10 ? '0' + day : '' + day;
                newArgs.hid = $('#selectedHost').val();
                newArgs.start = $('#startTime').val();
                newArgs.end = $('#endTime').val();

                if (newArgs.hid && newArgs.start && newArgs.end) {
                    calendarMdl.addAvailability(newArgs);
                }
            });
        }
    };

    /**
     * The args object can take values for the following keys
     * @param  aid
     * @param  el
     * @param  day
     */
    calendarMdl.deleteAvailability = function(args) {
        args = typeof args !== 'undefined' ? args : {};

        $.ajax({
            type : 'POST',
            url : '/calendar',
            data : {
                reqType : 'deleteAvail',
                aid : args.aid
            },
            dataType    : 'json',
            encode      : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            if (data.status == 'success') {
                var oldMarkup = '<div class="day-number">' + args.day + '</div>';
                oldMarkup += '<div class="day-icon-bar i2">';
                oldMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                oldMarkup += '<div class="icon i-sky add-booking"><i class="fa fa-plus"></i></div></div>';
                var theAid = args.el.attr('aid');
                for (var i = 0; i < serverData.avails.length; i++) {
                    if (serverData.avails[i]._id === theAid) {
                        serverData.avails.splice(i, 1);
                    }
                }
                args.el.removeClass('available')
                    .attr('aid', '')
                    .attr('start', '')
                    .attr('end', '')
                    .html(oldMarkup);
                var newArgs = {el : args.el.find('.add-booking')};
                calendarMdl.bindAddAvailability(newArgs);
            }
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindDeleteAvailability = function (args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};

        if (args.el) {
            $(args.el).click(function(){
                newArgs.el = $(this).parent().parent();
                newArgs.day = newArgs.el.find('.day-number').html();
                newArgs.aid = newArgs.el.attr('aid');

                if (newArgs.aid) {
                    calendarMdl.deleteAvailability(newArgs);
                }
            });
        } else {
            $('.remove-booking').click(function(){
                newArgs.el = $(this).parent().parent();
                newArgs.day = newArgs.el.find('.day-number').html();
                newArgs.aid = newArgs.el.attr('aid');

                if (newArgs.aid) {
                    calendarMdl.deleteAvailability(newArgs);
                }
            });
        }
    };

    /**
     * The args object can take values for the following keys
     * @param  month
     * @param  year
     * @param  day
     * @param  hid
     * @param  start
     * @param  end
     */
    calendarMdl.addBooking = function(args) {
        args = typeof args !== 'undefined' ? args : {};
        $.ajax({
            type        : 'POST',
            url         : '/calendar',
            data        :  {
                reqType : 'addBooking',
                hid : args.hid,
                aid : args.aid,
                uid : serverData.uid
            },
            dataType    : 'json',
            encode      : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            if (data.status == 'success') {
                serverData.bookings.push(data.booking);
                var newArgs = {
                    avail : data.avail,
                    booking : data.booking,
                    situ : 'bookingAdded'
                };
                calendarMdl.refreshBookingsServerData
                calendarMdl.processCalendarView(newArgs);
            }
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindAddBooking = function (args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};

        if (args.el) {
            args.el.click(function(){
                newArgs.hid = $(this).parent().parent().attr('data-hid');
                newArgs.aid = $(this).parent().parent().attr('data-aid');

                if (newArgs.hid && newArgs.aid) {
                    calendarMdl.addBooking(newArgs);
                }
            });
        } else {
            $('.add-booking').click(function(){
                newArgs.hid = $(this).parent().parent().attr('data-hid');
                newArgs.aid = $(this).parent().parent().attr('data-aid');
                if (newArgs.hid && newArgs.aid) {
                    calendarMdl.addBooking(newArgs);
                }
            });
        }
    };

    /**
     * The args object can take values for the following keys
     * @param  month
     * @param  year
     * @param  day
     * @param  hid
     * @param  start
     * @param  end
     */
    calendarMdl.deleteBooking = function(args) {
        args = typeof args !== 'undefined' ? args : {};
        $.ajax({
            type        : 'POST',
            url         : '/calendar',
            data        :  {
                reqType : 'deleteBooking',
                bid : args.bid
            },
            dataType    : 'json',
            encode      : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            if (data.status == 'success') {
                var newArgs = {
                    situ : 'bookingDeleted',
                    bid : args.bid,
                    aid : data.aid
                };
                calendarMdl.refreshBookingsServerData();
                calendarMdl.processCalendarView(newArgs);
            }
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindDeleteBooking = function (args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};
        if (args.el) {
            args.el.click(function(){
                newArgs.bid = $(this).parent().parent().attr('data-bid');
                if (newArgs.bid) {
                    calendarMdl.deleteBooking(newArgs);
                }
            });
        } else {
            $('.remove-booking').click(function(){
                newArgs.bid = $(this).parent().parent().attr('data-bid');
                if (newArgs.bid) {
                    calendarMdl.deleteBooking(newArgs);
                }
            });
        }
    };

    /**
     * The args object can take values for the following keys
     * @param  aid
     * @param  uid
     * @param  hid
     */
    calendarMdl.addWaitlist = function (args){
        vs.api.waitlist.getByAid(args.aid, function(waitlist){
            if (waitlist.length == 0){
                vs.api.waitlist.create({
                    aid: args.aid,
                    uid: args.uid,
                    hid: args.hid
                }, function(waitlist){
                    var newArgs = {
                        situ : 'waitlistAdded',
                        aid : waitlist.aid
                    };
                    calendarMdl.processCalendarView(newArgs);
                });
            } else {
                vs.api.waitlist.addUser({
                    aid: args.aid,
                    uid: args.uid
                }, function(waitlist){
                    var newArgs = {
                        situ : 'waitlistAdded',
                        aid : waitlist.aid
                    };
                    calendarMdl.processCalendarView(newArgs);
                });
            }
        });
    }

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindAddWaitlist = function (args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};

        if (args.el) {
            args.el.click(function(){
                newArgs.aid = $(this).parent().parent().attr('data-aid');
                newArgs.hid = $(this).parent().parent().attr('data-hid');
                newArgs.uid = serverData.uid;
                if (newArgs.aid) {
                    calendarMdl.addWaitlist(newArgs);
                }
            });
        } else {
            $('.add-waitlist').click(function(){
                newArgs.aid = $(this).parent().parent().attr('data-aid');
                newArgs.hid = $(this).parent().parent().attr('data-hid');
                newArgs.uid = serverData.uid;
                if (newArgs.aid) {
                    calendarMdl.addWaitlist(newArgs);
                }
            });
        }

    };

    calendarMdl.deleteWaitlist = function (args){
        vs.api.waitlist.getByAid(args.aid, function(waitlist){
            if (waitlist.length != 0){
                vs.api.waitlist.removeUser({
                    aid: args.aid,
                    uid: args.uid
                }, function(waitlist){
                    if (waitlist.users.length < 1){
                        vs.api.waitlist.remove(args.aid, function(){
                            var newArgs = {
                                situ : 'waitlistDeleted',
                                aid : args.aid
                            };
                            calendarMdl.processCalendarView(newArgs);
                        });
                    } else {
                        var newArgs = {
                            situ : 'waitlistDeleted',
                            aid : args.aid
                        };
                        calendarMdl.processCalendarView(newArgs);
                    }
                });
            }
        });
    }

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindDeleteWaitlist = function (args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};
        console.log(args);
        if (args.el) {
            args.el.click(function(){
                newArgs.aid = args.aid;
                newArgs.uid = serverData.uid;
                if (newArgs.aid) {
                    calendarMdl.deleteWaitlist(newArgs);
                }
            });
        } else {
            $('.remove-waitlist').click(function(){
                newArgs.aid = args.aid;
                newArgs.uid = serverData.uid;
                if (newArgs.aid) {
                    calendarMdl.deleteWaitlist(newArgs);
                }
            });
        }

    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.refreshBookingsServerData = function (){
        vs.api.booking.getByUid(serverData.uid, function(bookings){
            serverData.bookings = bookings;
        });
    };

    // MARKUP LOOPS AND ALTERATIONS

    /**
     * The args object uses values for the following keys
     * @param  avail
     */
    calendarMdl.loopShowAvailSetAvail = function(args, cb) {
        (function(){
            var newMarkup,
                $calendarCell,
                currentHID = $('#selectedHost').val();

            for (var i = 0; i < args.avail.length; i++) {
                if (args.avail[i].hid === currentHID) {
                    newMarkup = '<span class="avail-info"><strong>';
                    newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                    newMarkup += ' - ';
                    newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                    newMarkup += '</strong></span>';
                    newMarkup += '<div class="day-number">';
                    newMarkup += args.avail[i].day;
                    newMarkup += '</div>';
                    newMarkup += '<div class="day-icon-bar i2">';
                    newMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                    newMarkup += '<div class="icon i-coral remove-booking"><i class="fa fa-minus"></i></div></div>';

                    $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                    $calendarCell.addClass('available')
                      .attr('aid', args.avail[i]._id)
                      .attr('start', args.avail[i].start)
                      .attr('end', args.avail[i].end)
                      .html(newMarkup);
                }
            }
        })();
        if (cb) {
            cb();
        }
    };

    /**
     * The args object uses values for the following keys
     * @param  avail
     */
    calendarMdl.addOneAvailSetAvail = function(args, cb) {
        (function(){
            var newMarkup,
                newArgs = {},
                $calendarCell;

            newMarkup = '<span class="avail-info"><strong>';
            newMarkup += reference.hours[args.avail.start][0] + ' ' + reference.hours[args.avail.start][1];
            newMarkup += ' - ';
            newMarkup += reference.hours[args.avail.end][0] + ' ' + reference.hours[args.avail.end][1];
            newMarkup += '</strong></span>';
            newMarkup += '<div class="day-number">';
            newMarkup += args.avail.day;
            newMarkup += '</div>';
            newMarkup += '<div class="day-icon-bar i2">';
            newMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
            newMarkup += '<div class="icon i-coral remove-booking"><i class="fa fa-minus"></i></div></div>';

            $calendarCell = $('td[data-day="' + args.avail.day + '"]');
            $calendarCell.addClass('available')
                .attr('aid', args.avail._id)
                .attr('start', args.avail.start)
                .attr('end', args.avail.end)
                .html(newMarkup);

            newArgs.el = 'td[data-day="' + args.avail.day + '"] .remove-booking';
        })();
        if (cb) {
            cb(args);
        }
    };

    /**
     * The args object uses values for the following keys
     * @param  avail
     */
    calendarMdl.loopShowAvailUserProfile = function(args, cb) {
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.avail.length; i++) {
                $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');

                if (!$calendarCell.hasClass('available')) {
                    newMarkup = '<div class="day-number">' + args.avail[i].day + '</div>';
                    newMarkup += '<div class="day-icon-bar i2">';
                    newMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                    newMarkup += '<div class="icon i-sky view-avail"><i class="fa fa-search-plus"></i></div></div>';
                    $calendarCell.addClass('available')
                        .html(newMarkup);
                }
            }
        })();

        if (cb) {
            cb(args);
        }
    }

    /**
     * The args object uses values for the following keys
     * @param  avail
     */
    calendarMdl.loopShowAvailHostProfile = function(args, cb) {
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.avail.length; i++) {
                $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                newMarkup =  '<span class="avail-info"><strong>Unbooked Availability</strong>';
                newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                newMarkup += ' - ';
                newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                newMarkup += '</span>';
                newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';

                $calendarCell.addClass('available')
                    .attr('data-aid', args.avail[i]._id)
                    .html(newMarkup);
            }
        })();

        if (cb) {
            cb(args);
        }
    }

    /**
     * The args object uses values for the following keys
     * @param  avail
     */
    calendarMdl.loopShowBookedHostProfile = function(args, cb) {
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.bookings.length; i++) {
                $calendarCell = $('td[data-aid="' + args.bookings[i].aid + '"]');
                newMarkup =  '<span class="avail-info"><strong>Booked!</strong>';
                for (var j = 0; j < args.avail.length; j++) {
                    if (args.avail[j]._id == args.bookings[i].aid){
                        newMarkup += reference.hours[args.avail[j].start][0] + ' ' + reference.hours[args.avail[j].start][1];
                        newMarkup += ' - ';
                        newMarkup += reference.hours[args.avail[j].end][0] + ' ' + reference.hours[args.avail[j].end][1];
                        newMarkup += '</span>';
                        newMarkup += '<div class="day-number">' + args.avail[j].day + '</div>';
                        newMarkup += '<div class="day-icon-bar i2">';
                        newMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div></div>';
                    }
                }
                $calendarCell.addClass('available')
                    .attr('data-bid', args.bookings[i]._id)
                    .html(newMarkup);
            }
        })();

        if (cb) {
            cb(args);
        }
    }

    calendarMdl.loopShowBookedUserProfile = function(args, cb) {
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.avail.length; i++) {
                $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                for (var j = 0; j < args.booking.length; j++) {
                    if (args.avail[i]._id === args.booking[j].aid) {
                        for (var k = 0; k < serverData.hosts.length; k++) {
                            if (args.booking[j].hid == serverData.hosts[k]._id) {
                                var thisHost = serverData.hosts[k];
                            }
                        }
                        newMarkup = '<span class="booking-info"><strong>';
                        newMarkup += thisHost.company.name;
                        newMarkup += '<br/>';
                        newMarkup += '</strong>';
                        newMarkup += thisHost.company.city + ', ' + thisHost.company.state;
                        newMarkup += '<br/>';
                        newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                        newMarkup += ' - ';
                        newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                        newMarkup += '</span>';
                        newMarkup += '<div class="day-icon-bar i2">';
                        newMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                        newMarkup += '<div class="icon i-coral remove-booking"><i class="fa fa-minus"></i></div></div>';
                        newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';
                        $calendarCell.addClass('booked-you')
                            .attr('data-bid', args.avail[i].bid)
                            .html(newMarkup);
                    }
                }
            }
        })();

        if (cb) {
            cb(args);
        }
    }

    calendarMdl.loopShowAvailUserHost = function(args, cb){
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.avail.length; i++) {
                $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                if (!$calendarCell.hasClass('available') && args.hid === args.avail[i].hid) {

                    newMarkup =  '<span class="avail-info"><strong>Available:</strong>';
                    newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                    newMarkup += ' - ';
                    newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                    newMarkup += '</span>';
                    newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';
                    newMarkup += '<div class="day-icon-bar i2">';
                    newMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                    newMarkup += '<div class="icon i-sky add-booking"><i class="fa fa-plus"></i></div></div>';

                    $calendarCell.addClass('available')
                        .attr('data-aid', args.avail[i]._id)
                        .attr('data-hid', args.hid)
                        .html(newMarkup);
                }
            }
        })();

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.loopShowBookedUserHost = function(args, cb){
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.avail.length; i++) {
                if (args.avail[i].booked) {
                    $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                    if ($calendarCell.hasClass('available') && args.hid === args.avail[i].hid) {

                        newMarkup =  '<span class="avail-info"><strong>Already Booked</strong><br/>Join Waitlist?</span>';
                        newMarkup += '<div class="day-icon-bar i2">';
                        newMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                        newMarkup += '<div class="icon i-gray add-waitlist"><i class="fa fa-clock-o"></i></div></div>';
                        newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';

                        $calendarCell.removeClass('available').addClass('booked')
                            .html(newMarkup);
                    }
                }
            }
        })();

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.loopShowBookedSelfUserHost = function(args, cb){
        (function(){
            var newMarkup,
                $calendarCell;

                console.log(args);

            for (var i = 0; i < args.avail.length; i++) {
                if (args.avail[i].booked) {
                    $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                    if (args.hid === args.avail[i].hid && serverData.uid === args.avail[i].uid) {
                        newMarkup =  '<span class="avail-info"><strong>You\'re Booked!</strong>';
                        newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                        newMarkup += ' - ';
                        newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                        newMarkup += '</span>';
                        newMarkup += '<div class="day-icon-bar i2">';
                        newMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                        newMarkup += '<div class="icon i-coral remove-booking"><i class="fa fa-minus"></i></div></div>';
                        newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';

                        $calendarCell.removeClass('available').addClass('booked')
                            .attr('data-bid', args.avail[i].bid)
                            .html(newMarkup);
                    }
                }
            }
        })();

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.addOneBookingUserHost = function(args, cb){
        (function(){
            var newMarkup,
                $calendarCell;

            $calendarCell = $('td[data-day="' + args.avail.day + '"]');
            newMarkup =  '<span class="avail-info"><strong>You\'re Booked!</strong>';
            newMarkup += reference.hours[args.avail.start][0] + ' ' + reference.hours[args.avail.start][1];
            newMarkup += ' - ';
            newMarkup += reference.hours[args.avail.end][0] + ' ' + reference.hours[args.avail.end][1];
            newMarkup += '</span>';
            newMarkup += '<div class="day-icon-bar i2">';
            newMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
            newMarkup += '<div class="icon i-coral remove-booking"><i class="fa fa-minus"></i></div></div>';
            newMarkup += '<div class="day-number">' + args.avail.day + '</div>';

            $calendarCell.removeClass('available').addClass('booked')
                .attr('data-bid', args.avail.bid)
                .html(newMarkup);

        })();

        if (cb) {
            cb(args);
        }

    };

    calendarMdl.deleteOneBookingUserHost = function(args, cb){

        (function(args){
        var newMarkup,
            $calendarCell,
            thisHid = $('#current-hid').attr('data-hid');

            for (var i = 0; i < serverData.avails.length; i++) {
                if (serverData.avails[i]._id === args.aid) {
                    args.avail = serverData.avails[i];
                }
            }

            $calendarCell = $('td[data-bid="' + args.bid + '"]');

                newMarkup =  '<span class="avail-info"><strong>Available:</strong>';
                newMarkup += reference.hours[args.avail.start][0] + ' ' + reference.hours[args.avail.start][1];
                newMarkup += ' - ';
                newMarkup += reference.hours[args.avail.end][0] + ' ' + reference.hours[args.avail.end][1];
                newMarkup += '</span>';
                newMarkup += '<div class="day-icon-bar i2">';
                newMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                newMarkup += '<div class="icon i-sky add-booking"><i class="fa fa-plus"></i></div></div>';
                newMarkup += '<div class="day-number">' + args.avail.day + '</div>';

                $calendarCell.addClass('available')
                    .removeClass('booked')
                    .attr('data-aid', args.aid)
                    .attr('data-hid', thisHid)
                    .attr('data-bid', '')
                    .html(newMarkup);

                args.el = $('td[data-aid="' + args.aid + '"] .add-booking');
        })(args);

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.addOneWaitlistUserHost = function(args, cb){
        vs.api.availability.getById(args.aid, function(avail){
            (function(args){
                var newMarkup,
                    $calendarCell;

                    args.avail = avail;
                    $calendarCell = $('td[data-day="' + avail.day + '"]');
                    newMarkup =  '<span class="avail-info"><strong>On Waitlist</strong>';
                    newMarkup += '</span>';
                    newMarkup += '<div class="day-icon-bar i2">';
                    newMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                    newMarkup += '<div class="icon i-coral remove-waitlist"><i class="fa fa-minus"></i></div></div>';
                    newMarkup += '<div class="day-number">' + avail.day + '</div>';

                    $calendarCell.removeClass('booked').addClass('waitlisted')
                        .html(newMarkup);

                    args.el = $('td[data-day="' + avail.day + '"]').find('.remove-waitlist');
            })(args);

            if (cb) {
                cb(args);
            }
        });
    };

    calendarMdl.deleteOneWaitlistUserHost = function(args, cb){

        (function(args){
        var newMarkup,
            $calendarCell,
            thisHid = $('#current-hid').attr('data-hid');

            for (var i = 0; i < serverData.avails.length; i++) {
                if (serverData.avails[i]._id === args.aid) {
                    args.avail = serverData.avails[i];
                }
            }

            $calendarCell = $('td[data-aid="' + args.aid + '"]');

                newMarkup =  '<span class="avail-info"><strong>Available:</strong>';
                newMarkup += reference.hours[args.avail.start][0] + ' ' + reference.hours[args.avail.start][1];
                newMarkup += ' - ';
                newMarkup += reference.hours[args.avail.end][0] + ' ' + reference.hours[args.avail.end][1];
                newMarkup += '</span>';
                newMarkup += '<div class="day-icon-bar i2">';
                newMarkup += '<div class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                newMarkup += '<div class="icon i-gray add-waitlist"><i class="fa fa-clock-o"></i></div></div>';
                newMarkup += '<div class="day-number">' + args.avail.day + '</div>';

                $calendarCell.addClass('booked')
                    .removeClass('waitlisted')
                    .attr('data-aid', args.aid)
                    .attr('data-hid', thisHid)
                    .html(newMarkup);

                args.el = $('td[data-aid="' + args.aid + '"] .add-booking');
        })(args);

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.displaySideHostsWithAvail = function(args, cb){
        (function(){
            var newMarkup = '<h2 class="sidebar-title">Available Hosts</h2><div class="inner-sidebar">';
            newMarkup += '<span class="avail-statement">The following companies have availability on ';
            newMarkup += reference.months[serverData.currentMonth] + ' ' + args.day + ', ' + serverData.currentYear + ':</span>';
            newMarkup += '<ul class="availability-list">';
            for (var i = 0; i < args.avail.length; i++) {
                newMarkup += '<li data-hid="' + args.avail[i].hid + '">';
                newMarkup += '<h3 class="view-host-avail">' + args.avail[i].hostInfo.company.name + '</h3>';
                newMarkup += '<span class="host-info">$' + args.avail[i].hostInfo.company.fee + ' | ' + args.avail[i].hostInfo.company.city + ', ' + args.avail[i].hostInfo.company.state + '</span>';
                newMarkup += '</li>';
            }
            newMarkup += '</ul></div>';
            $('.sidebar').html(newMarkup);
        })();

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.displaySideHostInfo = function(args, cb){
        (function(){
            var newMarkup;
            for (var i = 0; i < serverData.hosts.length; i++) {
                if (serverData.hosts[i]._id === args.hid) {
                    //NAME
                    newMarkup = '<h2 class="sidebar-title">' + serverData.hosts[i].company.name + '</h2><div class="inner-sidebar">';
                    //ADDRESS
                    newMarkup += '<span class="host-info host-address"><strong>Address:</strong>' + serverData.hosts[i].company.address + '<br/>' + serverData.hosts[i].company.city + ', ' + serverData.hosts[i].company.state + ' ' + serverData.hosts[i].company.zip + '</span>';
                    //SIZE
                    newMarkup += '<span class="host-info host-size"><strong>Size:</strong>' + reference.companySize[serverData.hosts[i].company.size] + ' employees</span>';
                    //FEE
                    newMarkup += '<span class="host-info host-fee"><strong>Fee:</strong>$' + serverData.hosts[i].company.fee + '</span></div>';
                    newMarkup += '<div style="display:none" id="current-hid" data-hid="' + serverData.hosts[i]._id + '"></div>';
                }
            }
            $('.sidebar').html(newMarkup);
        })();

        if (cb) {
            cb(args);
        }
    };


    calendarMdl.bindSideHostAvail = function(args, cb){
        (function(args){
            var args1 = {},
                args2 = {};
            args1.month = serverData.currentMonth;
            args1.year = serverData.currentYear;
            args1.situ = 'load-host';
            args1.plus = false;

            $('.view-host-avail').unbind('click');
            $('.view-host-avail').click(function(){
                args1.hid = $(this).parent().attr('data-hid');
                args2.avail = args.avail;
                args2.situ = 'display-host';
                args2.hid = args1.hid;

                if (cb) {
                    cb(args1, args2);
                }
            });
        })(args);
    };
    /**
     * The args object can take values for the following keys
     * @param  avail
     * @param  hid
     * @param  situ
     */
    calendarMdl.processCalendarView = function(args){
        args = typeof args !== 'undefined' ? args : {};
        args.hid = typeof args.hid !== 'undefined' ? args.hid : '';
        var newArgs = {},
            availableDayMarkup;

        switch (args.situ) {
            case 'pageload-avail':
                calendarMdl.loopShowAvailSetAvail(args, function(){
                    calendarMdl.bindAddAvailability();
                    calendarMdl.bindDeleteAvailability();
                });
            break;

            case 'availAdded':
                calendarMdl.addOneAvailSetAvail(args, function(newArgs){
                    calendarMdl.bindDeleteAvailability(newArgs);
                });
            break;

            case 'pageload-user':
                calendarMdl.loopShowAvailUserProfile(args, function(args){
                    calendarMdl.loopShowBookedUserProfile(args, function(args){
                        calendarMdl.bindFindAvailability(args);
                        calendarMdl.bindDeleteBooking(args);
                    });
                });
            break;

            case 'pageload-host':
                calendarMdl.loopShowAvailHostProfile(serverData, function(args){
                    calendarMdl.loopShowBookedHostProfile(args);
                });
            break;

            case 'load-host':
                calendarMdl.loopShowAvailUserHost(args, function(args){
                    calendarMdl.loopShowBookedUserHost(args, function(args){
                        calendarMdl.loopShowBookedSelfUserHost(args, function(args){
                            calendarMdl.bindAddBooking(args);
                            calendarMdl.bindAddWaitlist(args);
                        });
                    });
                });
            break;

            case 'bookingAdded':
                calendarMdl.addOneBookingUserHost(args, function(args){
                    calendarMdl.bindDeleteBooking(args);
                });
            break;

            case 'bookingDeleted':
                calendarMdl.deleteOneBookingUserHost(args, function(args){
                    calendarMdl.bindAddBooking(args);
                });
            break;

            case 'waitlistAdded':
                calendarMdl.addOneWaitlistUserHost(args, function(args){
                    calendarMdl.bindDeleteWaitlist(args);
                });
            break;

            case 'waitlistDeleted':
                calendarMdl.deleteOneWaitlistUserHost(args, function(args){
                    calendarMdl.bindAddWaitlist(args);
                });
            break;
        }
    };

    /**
     * The args object can take values for the following keys
     * @param  avail
     * @param  hid
     * @param  situ
     */
    calendarMdl.processSidebarView = function(args){
        args = typeof args !== 'undefined' ? args : {};
        args.hid = typeof args.hid !== 'undefined' ? args.hid : '';
        var newArgs = {};

        switch (args.situ) {
            case 'display-avail':
                $('.calendarWrap').attr('data-calendar-view', '2');
                calendarMdl.displaySideHostsWithAvail(args, function(args){
                    calendarMdl.bindSideHostAvail(args, function(args1, args2){
                        calendarMdl.changeMonth(args1);
                        calendarMdl.processSidebarView(args2);
                    });
                });
            break;

            case 'display-host':
                $('.calendarWrap').attr('data-calendar-view', '3');
                calendarMdl.displaySideHostInfo(args);
            break;
        }
    };
});