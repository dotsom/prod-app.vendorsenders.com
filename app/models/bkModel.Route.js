var RouteMdl = {};
RouteMdl.isLoggedIn = function (req, res) {
    if (req.isAuthenticated()) {
        return true;
    } else {
        return false;
    }
};

RouteMdl.isAdmin = function (req, res){
    if (typeof req.user == 'undefined') {
        return false;
    } else {
        if (req.user.local.accountType == 99) {
            return true;
        } else {
            return false;
        }
    }
};

RouteMdl.isHost = function (req, res){
    if (typeof req.user == 'undefined') {
        return false;
    } else {
        if (req.user.local.accountType == 2) {
            return true;
        } else {
            return false;
        }
    }
};

module.exports = RouteMdl;