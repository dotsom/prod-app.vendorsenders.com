// app/models/availabilityModel.js
// load the things we need
var mongoose = require('mongoose');

var availabilitySchema = mongoose.Schema({
    hid : { type: String, required: true, trim: true },
    day : { type: Number, required: true, trim: true },
    month : { type: Number, required: true, trim: true },
    year : { type: Number, required: true, trim: true },
    start : { type: String, required: true, trim: true },
    end : { type: String, required: true, trim: true },
    booked : Boolean,
    uid : { type: String, required: false, trim: true },
    bid : { type: String, required: false, trim: true }
});

// create the model for users and expose it to our app
module.exports = mongoose.model('Availability', availabilitySchema, 'availabilities');