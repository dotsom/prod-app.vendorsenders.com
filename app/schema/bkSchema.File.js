var mongoose = require('mongoose');

var fileSchema = mongoose.Schema({
  uid : { type: String, required: true, trim: true },
  name : { type: String, required: true, trim: true },
  path : { type: String, required: true, trim: true },
  type : { type: String, required: true, trim: true },
  created : { type: Date, required: true, default: Date.now}
});

// create the model for users and expose it to our app
module.exports = mongoose.model('File', fileSchema);