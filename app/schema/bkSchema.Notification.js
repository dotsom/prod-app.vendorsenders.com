var mongoose = require('mongoose');
var notificationSchema = mongoose.Schema({
    uid : [],
    message : { type: String, required: true, trim: true },
    meta: {},
    messageType : { type: String, required: true, trim: true },
    read : { type: Boolean, required: true, trim: true },
    timeRead : {type: Date, required: false},
    timeCreated : { type: Date, required: true, default: Date.now}
});
module.exports = mongoose.model('Notification', notificationSchema);