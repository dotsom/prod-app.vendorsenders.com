var mongoose = require('mongoose');
var pwtokenSchema = mongoose.Schema({
    token : { type: String, required: true, trim: true },
    expires : { type: Date, required: true, trim: true },
    uid : { type: String, required: true, trim: true }
});
module.exports = mongoose.model('PwToken', pwtokenSchema, 'pwtokens');