module.exports = function(
    app,
    passport,
    mongoose,
    Availability
) {

    //GET request for all availabilities
    app.get('/api/availability', function(req, res) {
        Availability.find(function(err, availabilities){
            if (err) {
                res.send(err);
            } else {
                res.json(availabilities);
            }
        });
    });

    //GET request for a single availability
    app.get('/api/availability/:_id', function(req, res) {
        Availability.findById(req.params._id, function(err, availability){
            if (err) {
                res.send(err);
            } else {
                res.json(availability);
            }
        });
    });

    //GET request for a single year
    app.get('/api/availability/year/:year', function(req, res) {
        Availability.find({
            year : parseInt(req.params.year, 10)
        }, function(err, availabilities){
            if (err) {
                res.send(err);
            } else {
                res.json(availabilities);
            }
        });
    });

    //GET request for a single month
    app.get('/api/availability/year/:year/month/:month', function(req, res) {
        Availability.find({
            month : parseInt(req.params.month, 10),
            year : parseInt(req.params.year, 10)
        }, function(err, availabilities){
            if (err) {
                res.send(err);
            } else {
                res.json(availabilities);
            }
        });
    });
};