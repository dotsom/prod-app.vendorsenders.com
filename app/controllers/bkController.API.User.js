module.exports = function(
    app,
    passport,
    mongoose,
    validator,
    _,
    encoder,
// Schema
    Availability,
    Booking,
    Host,
    User,
// Models
    CalendarMdl,
    NotificationMdl,
    RouteMdl
) {

    //GET request for all users
    app.get('/api/users', function(req, res) {
        User.find(function(err, users){
            if (err) {
                res.send(err);
            } else {
                var allUsers = [];
                _.each(users, function(user){
                    var thisUser = {
                        _id: user._id,
                        __v: user.__v,
                        local: {
                            accountStatus: user.local.accountStatus,
                            accountType: user.local.accountType,
                            phone: user.local.phone,
                            lastName: user.local.lastName,
                            firstName: user.local.firstName,
                            email: user.local.email
                        }
                    }
                    if (typeof user.bus != 'undefined') {
                        thisUser.bus = user.bus;
                    }
                    allUsers.push(thisUser);
                });
                res.json(allUsers);
            }
        });
    });

    //GET request for a single user
    app.get('/api/users/:_id', function(req, res) {
        User.findById(req.params._id, function(err, user){
            if (err) {
                res.send(err);
            } else {
                var thisUser = {
                    _id: user._id,
                    __v: user.__v,
                    local: {
                        accountStatus: user.local.accountStatus,
                        accountType: user.local.accountType,
                        phone: user.local.phone,
                        lastName: user.local.lastName,
                        firstName: user.local.firstName,
                        email: user.local.email
                    }
                }
                if (typeof user.bus != 'undefined') {
                    thisUser.bus = user.bus;
                }
                res.json(thisUser);
            }
        });
    });

    // POST a new user
    app.post('/api/users', function(req, res) {
        process.nextTick(function() {
            User.findOne({ 'local.email' :  req.body.email }, function(err, user) {
                // error
                if (err) {
                    res.location('/signup');
                    res.send(200, {signupErrorId: 1, err: err});
                }

                // already taken
                if (user) {
                    res.location('/signup');
                    res.send(200, {signupErrorId: 2, err: 'That email address is already in use.'});
                }

                // create user
                else {
                    // if there is no user with that email
                    // create the user
                    var newUser            = new User(),
                        errorCount         = 0,
                        errorMsgs          = [];

                    // set the user's local credentials
                    // sanitize as we go.

                    if (validator.isEmail(validator.toString(req.body.email))) {
                        newUser.local.email = encoder.htmlEncode(String(req.body.email));
                        console.log("Valid email!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter a valid email address.');
                    }

                    if (validator.isLength(req.body.password, 8)) {
                        newUser.local.password = newUser.generateHash(req.body.password);
                        console.log("Valid password!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Passwords must be at least 8 characters.');
                    }

                    if (req.body.firstName != "") {
                        newUser.local.firstName = encoder.htmlEncode(String(req.body.firstName));
                        console.log("Valid first name!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter your first name.');
                    }

                    if (req.body.lastName != "") {
                        newUser.local.lastName = encoder.htmlEncode(String(req.body.lastName));
                        console.log("Valid last name!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter your last name.');
                    }

                    if (req.body.phone != "") {
                        newUser.local.phone = encoder.htmlEncode(String(req.body.phone));
                        console.log("Valid phone number!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter a valid phone number.');
                    }

                    newUser.local.accountType       = Number(1);
                    newUser.local.accountStatus     = Number(1);

                    // save the user
                    if (errorCount == 0) {
                        console.log('New User:\n', newUser);
                        newUser.save(function(err) {
                            if (err) {
                                res.send(200, {signupErrorId: 3, err: err});
                                console.log(err);
                            } else {
                                res.send(200, {signupErrorId: false, user: newUser});
                                console.log("Successful Save!");
                            }
                        });
                    } else {
                        res.send(200, {signupErrorId: 4, err: errorMsgs});
                    }
                }
            });
        });
    });

    //PUT request to update a user
    app.put('/api/users/:_id', function(req, res) {
        User.findById(req.params._id, function(err, user){
            if (err) {
                res.send(err);
            } else {
                if (req.body.accountStatus) {
                    user.local.accountStatus = Number(req.body.accountStatus);
                }
                if (req.body.accountType) {
                    user.local.accountType = Number(req.body.accountType);
                }
                if (req.body.phone) {
                    if (req.body.phone != "") {
                        user.local.phone = encoder.htmlEncode(String(req.body.phone));
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter a valid phone number.');
                    }
                }
                if (req.body.firstName) {
                    if (req.body.firstName != "") {
                        user.local.firstName = encoder.htmlEncode(String(req.body.firstName));
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter your first name.');
                    }
                }
                if (req.body.lastName) {
                    if (req.body.lastName != "") {
                        user.local.lastName = encoder.htmlEncode(String(req.body.lastName));
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter your last name.');
                    }
                }
                if (req.body.email) {
                    if (validator.isEmail(validator.toString(req.body.email))) {
                        user.local.email = encoder.htmlEncode(String(req.body.email));
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter a valid email address.');
                    }
                }

                if (req.body.password) {
                    if (validator.isLength(req.body.password, 8)) {
                        user.local.password = user.generateHash(req.body.password);
                    } else {
                        errorCount++;
                        errorMsgs.push('Passwords must be at least 8 characters.');
                    }
                }

                if (typeof user.bus != 'undefined') {
                    if (req.body.bname) {
                        if (req.body.bname != "") {
                            var userbusname = encoder.htmlEncode(String(req.body.bname));
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter your business name.');
                        }
                    } else if (typeof user.bus.name != 'undefined') {
                        var userbusname = user.bus.name;
                    } else {
                        var userbusname = "";
                    }
                    if (req.body.binfo) {
                        if (req.body.binfo != "") {
                            userbusinfo = encoder.htmlEncode(String(req.body.binfo));
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a business description.');
                        }
                    } else if (typeof user.bus.info != 'undefined') {
                        var userbusinfo = user.bus.info;
                    } else {
                        var userbusinfo = "";
                    }
                    if (req.body.btype) {
                        if (req.body.btype != "") {
                            userbustype = encoder.htmlEncode(String(req.body.btype));
                        } else {
                            errorCount++;
                            errorMsgs.push('Please select a business type.');
                        }
                    } else if (typeof user.bus.type != 'undefined') {
                        var userbustype = user.bus.type;
                    } else {
                        var userbustype = "";
                    }
                } else {
                    if (req.body.bname) {
                        if (req.body.bname != "") {
                            var userbusname = encoder.htmlEncode(String(req.body.bname));
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter your business name.');
                        }
                    } else {
                        var userbusname = "";
                    }
                    if (req.body.binfo) {
                        if (req.body.binfo != "") {
                            userbusinfo = encoder.htmlEncode(String(req.body.binfo));
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a business description.');
                        }
                    } else {
                        var userbusinfo = "";
                    }
                    if (req.body.btype) {
                        if (req.body.btype != "") {
                            userbustype = encoder.htmlEncode(String(req.body.btype));
                        } else {
                            errorCount++;
                            errorMsgs.push('Please select a business type.');
                        }
                    } else {
                        var userbustype = "";
                    }
                }

                user.bus = {
                    name: userbusname,
                    info: userbusinfo,
                    type: userbustype
                };

                user.save(function(err){
                    if (err) {
                        res.send(err);
                    } else {
                        res.json({
                            message: "The update was successful",
                            user: user
                        });
                    }
                });
            }
        });
    });

    //PUT request to update a user
    app.delete('/api/users/:_id', function(req, res) {
        User.remove({
            _id: req.params._id
        }, function(err, user){
            if (err) {
                res.send(err);
            } else {
                res.json({
                    message: 'Successfully deleted'
                });
            }
        });
    });
};