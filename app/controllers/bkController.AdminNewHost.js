module.exports = function(
        app,
        validator,
    // Schema
        Host,
        User,
    // Models
        RouteMdl
    ) {

    app.get('/admin/new-host', function(req, res) {
        if (RouteMdl.isAdmin(req, res)){
            res.render('admin-new-host.ejs', {
                user: req.user,
                message: req.flash('signupMessage')
            });
        } else {
            res.redirect('/');
        }
    });
};