module.exports = function(
        app,
    // Schema
        Host,
        Availability,
        Booking,
    // Models
        CalendarMdl,
        RouteMdl
    ) {

    var ObjectId = require('mongoose').Types.ObjectId;
    var changedMonth;

    app.get('/calendar', function(req, res) {
        var serverData = {};
        var d = new Date();
        var cal_string = CalendarMdle.createMarkup(d.getMonth(), d.getFullYear());
        var cal_heading_string = CalendarMdl.monthNames[d.getMonth()] + " " + d.getFullYear();

        res.render('calendar.ejs', {
            data: JSON.stringify({
                Calendar: CalendarMdl,
                currentYear: d.getFullYear(),
                currentMonth: d.getMonth()
            }),
            calendar: cal_string,
            calendarHeading: cal_heading_string
        });
    });

    /**
     * Handle POST Requests
     */
    app.post('/calendar', function(req, res) {
        if (RouteMdl.isLoggedIn(req, res)){
            process.nextTick(function() {
                switch (req.body.reqType) {

                    // CHANGE MONTH ON CALENDAR
                    case "changeMonth":
                        changedMonth = CalendarMdl.changeMonth(Number(req.body.currentMonth),Number(req.body.currentYear));
                        if (changedMonth.newCalendarMdl) {
                            Availability.find({
                                month : changedMonth.newMonth,
                                year : changedMonth.newYear
                            }, function(err, availabilities){
                                changedMonth.avails = availabilities;
                                res.send(200, changedMonth);
                            });
                        }
                    break;

                    case "changeMonthNoPlus":
                        changedMonth = CalendarMdl.changeMonth(Number(req.body.currentMonth),Number(req.body.currentYear), false);
                        if (changedMonth.newCalendar) {
                            Availability.find({
                                month : changedMonth.newMonth,
                                year : changedMonth.newYear
                            }, function(err, availabilities){
                                changedMonth.avails = availabilities;
                                res.send(200, changedMonth);
                            });
                        }
                    break;

                    case "changeMonthArrow":
                        changedMonth = CalendarMdl.changeMonthArrow(Number(req.body.currentMonth),Number(req.body.currentYear), req.body.direction);
                        if (changedMonth.newCalendar) {
                            Availability.find({
                                month : changedMonth.newMonth,
                                year : changedMonth.newYear
                            }, function(err, availabilities){
                                changedMonth.avails = availabilities;
                                res.send(200, changedMonth);
                            });
                        }
                    break;

                    case "changeMonthArrowNoPlus":
                        changedMonth = CalendarMdl.changeMonthArrow(Number(req.body.currentMonth),Number(req.body.currentYear), req.body.direction, false);
                        if (changedMonth.newCalendar) {
                            Availability.find({
                                month : changedMonth.newMonth,
                                year : changedMonth.newYear
                            }, function(err, availabilities){
                                changedMonth.avails = availabilities;
                                res.send(200, changedMonth);
                            });
                        }
                    break;

                    // ADD NEW AVAILABILITY
                    case "addAvail":
                        var newAvail = new Availability();
                        newAvail.month = req.body.month;
                        newAvail.year = req.body.year;
                        newAvail.day = req.body.day;
                        newAvail.hid = req.body.hid;
                        newAvail.start = req.body.start;
                        newAvail.end = req.body.end;
                        newAvail.booked = false;

                        newAvail.save(function(err) {
                            if (err) {
                                res.send(200, {err: err, status: "failure"});
                                console.log(err);
                            } else {
                                res.send(200, {avail: newAvail, status: "success"});
                            }
                        });
                    break;

                    // DELETE AVAILABILITY
                    case "deleteAvail":
                        Availability.remove({
                            _id : ObjectId(req.body.aid)
                        }, function(err){
                            if (err) {
                                res.send(200, {err: err, status: "failure"});
                                console.log(err);
                            } else {
                                res.send(200, {status: "success"});
                            }
                        });
                    break;

                    // ADD NEW BOOKING
                    case "addBooking":
                        process.nextTick(function() {
                            Booking.findOne({ 'aid' :  req.body.aid }, function(err, booking) {
                                if (err) {
                                    res.send(200, {errorId: 1, err: err});
                                } else if (booking) {
                                    res.send(200, {errorId: 2, err: 'That availability has already been booked.'});
                                } else {
                                    var newBooking = new Booking();
                                    newBooking.aid = req.body.aid;
                                    newBooking.hid = req.body.hid;
                                    newBooking.uid = req.body.uid;

                                    newBooking.save(function(err) {
                                        if (err) {
                                            res.send(200, {err: err, status: "failure"});
                                            console.log(err);
                                        } else {
                                            Availability.findOne({
                                                _id : newBooking.aid
                                            }, function(err, availability){
                                                availability.booked = true;
                                                availability.uid = req.body.uid;
                                                availability.bid = newBooking._id;
                                                availability.save(function(err){
                                                    if (err) {
                                                        res.send(200, {err: err, status: "failure"});
                                                        console.log(err);
                                                    } else {
                                                        res.send(200, {
                                                            avail : availability,
                                                            booking: newBooking,
                                                            status: "success"
                                                        });
                                                    }
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                        });
                    break;

                    // DELETE BOOKING
                    case "deleteBooking":
                        process.nextTick(function() {
                            Booking.findOne({ _id : ObjectId(req.body.bid)}, function(err, booking) {
                                if (booking) {
                                    var thisAid = booking.aid;
                                    if (err) {
                                        res.send(200, {errorId: 1, err: err});
                                    } else if (!booking) {
                                        res.send(200, {errorId: 2, err: 'A booking with that ID doesn does not exist.'});
                                    } else {
                                        booking.remove(function(err){
                                            if (err) {
                                                res.send(200, {err: err, status: "failure"});
                                                console.log(err);
                                            } else {
                                                Availability.update({
                                                    _id : thisAid
                                                }, {
                                                    booked : false,
                                                    uid : '',
                                                    bid : ''
                                                }, function(err, num){
                                                    if (err) {
                                                        res.send(200, {err: err, status: "failure"});
                                                        console.log(err);
                                                    } else {
                                                        res.send(200, {
                                                            aid: thisAid,
                                                            status: "success"
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                } else {
                                    //FIXME ERROR HANDLING
                                }
                            });
                        });
                    break;
                }
            });
        } else {

        }
    });
};