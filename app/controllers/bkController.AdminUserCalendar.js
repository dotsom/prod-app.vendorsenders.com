module.exports = function(
    app,
    passport,
    mongoose,
    validator,
    _,
    encoder,
// Schema
    Availability,
    Booking,
    Host,
    User,
// Models
    CalendarMdl,
    RouteMdl
) {

    //GET request for a single user
    app.get('/admin/user-calendar/:_id', function(req, res) {
        // Ensure admin authentication
        if (RouteMdl.isAdmin(req, res)){

            var serverData = {};
            var d = new Date();

            //Find specified user, set all values to thisUser object
            User.findById(req.params._id, function(err, user){
                    if (err) {
                        res.send(err);
                    } else {
                        var thisUser = {
                            _id: user._id,
                            __v: user.__v,
                            local: {
                                accountStatus: user.local.accountStatus,
                                accountType: user.local.accountType,
                                phone: user.local.phone,
                                lastName: user.local.lastName,
                                firstName: user.local.firstName,
                                email: user.local.email
                            }
                        }
                        // Handle .bus weirdness
                        if (typeof user.bus != 'undefined') {
                            thisUser.bus = user.bus;
                        }

                        // Get a list of all hosts
                        Host.find(function(err, hosts){
                            serverData.hosts = hosts;
                            Availability.find({
                                month : d.getMonth(),
                                year : d.getFullYear()
                            }, function(err, availabilities){
                                Booking.find({
                                    uid : thisUser._id
                                }, function(err, bookings){
                                    var cal_string = CalendarMdl.createMarkupNoPlus(d.getMonth(), d.getFullYear());
                                    var cal_heading_string = CalendarMdl.monthNames[d.getMonth()] + " " + d.getFullYear();

                                    serverData.avails = availabilities;
                                    serverData.bookings = bookings;
                                    serverData.uid = thisUser._id;
                                    serverData.Calendar = CalendarMdl;
                                    serverData.currentYear = d.getFullYear();
                                    serverData.currentMonth = d.getMonth();
                                    serverData.calendarString = cal_string;

                                    var renderData = {
                                        user : req.user,
                                        calendar : cal_string,
                                        calendarHeading : cal_heading_string,
                                        JSONdata : JSON.stringify(serverData),
                                        serverData : serverData
                                    };
                                    res.render('admin-user-calendar.ejs', renderData);
                                });
                            });
                        });
                    }
                });
        } else {
            res.redirect('/');
        }
    });
};