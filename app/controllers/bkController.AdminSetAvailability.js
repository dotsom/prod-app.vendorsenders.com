module.exports = function(
    app,
// Schema
    Availability,
    Host,
// Models
    CalendarMdl,
    RouteMdl
) {
    var ObjectId = require('mongoose').Types.ObjectId;
    var changedMonth;

    /**
     * Handle GET Requests
     */
    app.get('/admin/set-availability', function(req, res) {
        if (RouteMdl.isAdmin(req, res)) {
            var serverData = {};
            var d = new Date();

            Host.find(function(err, hosts){
                serverData.hosts = hosts;
                Availability.find({
                    month : d.getMonth(),
                    year : d.getFullYear()
                }, function(err, availabilities){
                    var cal_string = CalendarMdl.createMarkup(d.getMonth(), d.getFullYear());
                    var cal_heading_string = CalendarMdl.monthNames[d.getMonth()] + " " + d.getFullYear();

                    serverData.avails = availabilities;
                    serverData.Calendar = CalendarMdl;
                    serverData.currentYear = d.getFullYear();
                    serverData.currentMonth = d.getMonth();
                    serverData.calendarString = cal_string;

                    var renderData = {
                        user : req.user,
                        calendar : cal_string,
                        calendarHeading : cal_heading_string,
                        JSONdata : JSON.stringify(serverData),
                        serverData : serverData
                    };
                    res.render('admin-set-availability.ejs', renderData);
                });
            });
        } else {
            res.redirect('/');
        }
    });
};