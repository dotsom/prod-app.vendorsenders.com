// app/controllers/passport.js

module.exports = function(
    passport,
    localStrategy,
    bcrypt,
// Schema
    User,
    Host
) {

    // LOGIN AUTH
    passport.use('local-login', new localStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    },  function(req, email, password, done) {

        // Check if login exists
        User.findOne({ 'local.email' :  email }, function(err, user) {

            if (err)
                return done(err);
            // No username
            if (!user)
                return done(null, false, req.flash('loginMessage', 'The username/password combination you entered is incorrect.'));
            // Wrong password
            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'The username/password combination you entered is incorrect.'));

            // all is well, return successful user
            return done(null, user);
        });
    }));

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
};