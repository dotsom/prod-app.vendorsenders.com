module.exports = function(
    app,
    passport,
    mongoose,
    validator,
    _,
    encoder,
// Schema
    Availability,
    Booking,
    Host,
    Waitlist,
    User,
// Models
    CalendarMdl,
    NotificationMdl,
    RouteMdl
) {

    //GET request for all waitlists
    app.get('/api/waitlist', function(req, res) {
        Waitlist.find(function(err, waitlists){
            if (err) {
                res.send(err);
            } else {
                res.json(waitlists);
            }
        });
    });

    //GET request for a single waitlist by aid
    app.get('/api/waitlist/aid/:aid', function(req, res) {
        Waitlist.findOne({
            aid: req.params.aid
        }, function(err, waitlist){
            if (err){
                res.send({ error: true, message: 'There was an error fetching the waitlist', dump: err });
            }
            if (waitlist){
                res.json(waitlist);
            } else {
                res.json([]);
            }
        });
    });

    //GET request for a single waitlist by _id
    app.get('/api/waitlist/:_id', function(req, res) {
        Waitlist.findById(req.params._id, function(err, waitlist){
            if (err){
                res.send(err);
            }
            res.json(waitlist);
        });
    });

    // POST a new waitlist
    app.post('/api/waitlist', function(req, res) {
        Waitlist.findOne({ aid :  req.body.aid }, function(err, waitlist) {
            // error
            if (err) {
                res.location('/signup');
                res.send(err);
            }
            // already taken
            if (waitlist) {
                res.location('/signup');
                res.send({err: 'That availability already has an associated waitlist.'});
            }
            // create user
            else {
                var newWaitlist = new Waitlist();
                Availability.findById(req.body.aid, function(err, availability){
                    if (err) {
                        res.send(err);
                    } else if (availability){
                        newWaitlist.aid = req.body.aid;
                        Host.findById(req.body.hid, function(err, host){
                            if (err) {
                                res.send(err);
                            } else if (host){
                                newWaitlist.hid = req.body.hid;
                                User.findById(req.body.uid, function(err, user){
                                    if (err) {
                                        res.send(err);
                                    } else if (user){
                                        newWaitlist.users = [req.body.uid];
                                        newWaitlist.save(function(err) {
                                            if (err) {
                                                res.send(err);
                                            } else {
                                                res.send(200, { error: false, waitlist: newWaitlist});
                                            }
                                        });
                                    } else {
                                        res.send({error: true, message: 'There was no user with that _id found.'});
                                    }
                                });
                            } else {
                                res.send({error: true, message: 'There was no host with that _id found.'});
                            }
                        });
                    } else {
                        res.send({error: true, message: 'There was no availability with that _id found.'});
                    }
                });
            }
        });
    });

    // PUT a new user on waitlist
    app.put('/api/waitlist/add-user', function(req, res) {
        Waitlist.findOne({ aid :  req.body.aid }, function(err, waitlist) {
            // error
            if (err) {
                res.send(err);
            }
            if (waitlist) {
                User.findById(req.body.uid, function(err, user){
                    if (err) {
                        res.send(err);
                    } else if (user){
                        waitlist.users.push(user._id);
                        waitlist.save(function(err) {
                            if (err) {
                                res.send(err);
                            } else {
                                res.send(200, waitlist);
                            }
                        });
                    }
                });
            }
            else {
                res.location('/signup');
                res.send({err: 'No waitlist exists for that availability.'});
            }
        });
    });

    // PUT a new user on waitlist
    app.put('/api/waitlist/remove-user', function(req, res) {
        Waitlist.findOne({ aid :  req.body.aid }, function(err, waitlist) {
            // error
            if (err) {
                res.send(err);
            }
            if (waitlist) {
                User.findById(req.body.uid, function(err, user){
                    if (err) {
                        res.send(err);
                    } else if (user){
                        var i = waitlist.users.indexOf(user._id);
                        if (i > -1) {
                            waitlist.users.splice(i, 1);
                            waitlist.save(function(err) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    res.send(200, waitlist);
                                }
                            });
                        } else {
                            console.log('There is no user of that UID in the waitlist');
                        }
                        waitlist.save(function(err) {
                            if (err) {
                                res.send(err);
                            } else {
                                res.send(200, waitlist);
                            }
                        });
                    }
                });
            }
            else {
                res.location('/signup');
                res.send({err: 'No waitlist exists for that availability.'});
            }
        });
    });

    //DELETE request to remove a waitlist
    app.delete('/api/waitlist/:aid', function(req, res) {
        Waitlist.remove({
            aid: req.params.aid
        }, function(err){
            if (err) {
                res.send(err);
            } else {
                res.json({
                    message: 'Successfully deleted'
                });
            }
        });
    });
};