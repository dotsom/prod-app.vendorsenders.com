module.exports = function(
    app,
    passport,
    mongoose,
    validator,
    $,
// Schema
    Host,
    User,
// Models
    RouteMdl
) {

    var extend = require('extend')

    //GET request for all hosts
    app.get('/api/hosts', function(req, res) {
        Host.find(function(err, hosts){
            if (err) {
                res.send(err);
            } else {
                res.json(hosts);
            }
        });
    });

    //GET request for a single host
    app.get('/api/hosts/:_id', function(req, res) {
        Host.findById(req.params._id, function(err, host){
            if (err) {
                res.send(err);
            } else {
                res.json(host);
            }
        });
    });

    app.post('/api/hosts', function(req, res) {
        if (RouteMdl.isAdmin(req, res)){
            process.nextTick(function() {
                Host.findOne({ 'profile.email': req.body.email }, function(err, host) {
                    // error
                    if (err) {
                        res.location('/new-host');
                        res.send(200, {signupErrorId: 1, err: err});
                    }

                    // already taken
                    if (host) {
                        res.location('/new-host');
                        res.send(200, {signupErrorId: 2, err: 'That email address already has a company associated with it.'});
                    }

                    // create host
                    else {
                        var newHost            = new Host(),
                            errorCount         = 0,
                            errorMsgs          = [];

                        // set the user's local credentials
                        // sanitize as we go.
                        //
                        console.log(req.body);

                        if (validator.isEmail(validator.toString(req.body.email))) {
                            newHost.profile.email = String(req.body.email);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a valid email address.');
                        }

                        if (validator.isLength(req.body.password, 8)) {
                            newHost.profile.password = newHost.generateHash(req.body.password);
                        } else {
                            errorCount++;
                            errorMsgs.push('Passwords must be at least 8 characters.');
                        }

                        if (req.body.name != "") {
                            newHost.company.name = String(req.body.name);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a name for this company.');
                        }

                        if (req.body.phone != "") {
                            newHost.company.phone = String(req.body.phone);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a valid phone number.');
                        }

                        if (req.body.address != "") {
                            newHost.company.address = String(req.body.address1);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter an address for this company.');
                        }

                        if (req.body.city != "") {
                            newHost.company.city = String(req.body.city);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a city for this company.');
                        }

                        if (req.body.state != "") {
                            newHost.company.state = String(req.body.state);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a state for this company.');
                        }

                        if (req.body.zip != "") {
                            newHost.company.zip = String(req.body.zip);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a zip code for this company.');
                        }

                        if (req.body.fee != "") {
                            newHost.company.fee = Number(req.body.fee);
                        }

                        if (req.body.size != "") {
                            newHost.company.size = Number(req.body.size);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a size for this company.');
                        }

                        if (req.body.max != "") {
                            newHost.restrictions.max = Number(req.body.max);
                        }

                        newHost.profile.accountStatus     = Number(1);

                        // save the host
                        if (errorCount == 0) {
                            console.log('New Host:\n', newHost);
                            newHost.save(function(err) {
                                if (err) {
                                    res.send(200, {signupErrorId: 5, err: errorMsgs});
                                } else {
                                    User.findOne({'local.email': newHost.profile.email}, function(err, user){
                                        if (!user) {
                                            var newUser = new User();
                                            newUser.local = {
                                                accountStatus: 2,
                                                accountType: 2,
                                                phone: newHost.company.phone,
                                                firstName: 'Host',
                                                lastName: 'Company',
                                                password: newHost.profile.password,
                                                email: newHost.profile.email
                                            };
                                            newUser.bus = {
                                                hid: newHost._id
                                            };
                                            newUser.save(function(err){
                                                if (err) {
                                                    res.send(200, {signupErrorId: 3, err: errorMsgs});
                                                } else {
                                                    res.send(200, {signupErrorId: false, host: newHost, user: newUser});
                                                }
                                            });
                                        } else {
                                            res.send(200, {signupErrorId: 6, err: 'That email is already associated with an account.'});
                                        }
                                    });
                                }
                            });
                        } else {
                            res.send(200, {signupErrorId: 4, err: errorMsgs});
                        }
                    }
                });
            });
        } else {
            res.send(200, {signupErrorId: 5, err: 'You do not have the correct permissions to complete this request.'});
        }
    });

    app.put('/api/hosts/:_id', function(req, res) {
        //if (RouteMdl.isAdmin(req, res)){
            process.nextTick(function() {
                Host.findById(req.params._id, function(err,host){
                    if (err) {
                        res.send(err);
                    } else {
                        extend(host.profile, req.body.profile);
                        extend(host.company, req.body.company);

                        host.save(function(err){
                            if (err) {
                                res.send(200, {errorId: 5, err: err});
                            } else {
                                User.findOne({bus: {hid: host._id}}, function(err, user){
                                    if (err) {
                                        res.send(200, {errorId: 5, err: err});
                                    } else {
                                        var extender = {
                                                email: host.profile.email,
                                                phone: host.company.phone,
                                                password: host.profile.password
                                        };

                                        extend(user.local, extender);
                                        console.log(extender);
                                        console.log(user);

                                        user.save(function(err){
                                            if (err) {
                                                res.send(200, {errorId: 5, err: err});
                                            } else {
                                                res.send(200, {errorId: false, user: user, host: host});
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            });
        //} else {
            //res.send(200, {errorId: 5, err: 'You do not have the correct permissions to complete this request.'});
        //}
    });
};